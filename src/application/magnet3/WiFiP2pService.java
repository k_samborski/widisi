
package application.magnet3;

import wifidirect.p2pcore.WifiP2pDevice;



/**
 * A structure to hold service information.
 */
public class WiFiP2pService {
	public WifiP2pDevice device;
	public String instanceName = null;
	public String serviceRegistrationType = null;
}
