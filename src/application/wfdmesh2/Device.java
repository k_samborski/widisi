package application.wfdmesh2;

import java.util.HashMap;

import wifidirect.p2pcore.WifiP2pDevice;

public class Device {
	
	//Device Status
	public static final int CONNECTED   = 0;
	public static final int INVITED     = 1;
	public static final int FAILED      = 2;
	public static final int AVAILABLE   = 3;
	public static final int UNAVAILABLE = 4;
	
	public String deviceType;
	public String concurrentMode;
	public String address;
	public int clientsN;
	public int devicesN;
	public WifiP2pDevice p2pDevice;
	public HashMap<String, Device> localNeighborList;  			// Device info of discovered devices
	
	public Device(String address, String concurrentMode, String deviceType, WifiP2pDevice p2pDevice) {
		this.deviceType = deviceType;
		this.concurrentMode = concurrentMode;
		this.address = address;
		this.clientsN = 0;
		this.devicesN = 0;
		this.p2pDevice = p2pDevice;
		this.localNeighborList = new HashMap<String, Device>();
	}

}
