package application.wfdmesh2;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import application.magnet3.WiFiP2pService;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import visualization.Visualizer;
import wifi.ScanResult;
import wifi.WifiManager;
import wifidirect.nodemovement.NodeMovement;
import wifidirect.p2pcore.BroadcastReceiver;
import wifidirect.p2pcore.Callback;
import wifidirect.p2pcore.WifiP2pConfig;
import wifidirect.p2pcore.WifiP2pDevice;
import wifidirect.p2pcore.WifiP2pDeviceList;
import wifidirect.p2pcore.WifiP2pGroup;
import wifidirect.p2pcore.WifiP2pInfo;
import wifidirect.p2pcore.WifiP2pManager;
import wifidirect.p2pcore.WifiP2pManager.ConnectionInfoListener;
import wifidirect.p2pcore.WifiP2pManager.DnsSdServiceResponseListener;
import wifidirect.p2pcore.WifiP2pManager.DnsSdTxtRecordListener;
import wifidirect.p2pcore.WifiP2pManager.GroupInfoListener;
import wifidirect.p2pcore.WifiP2pManager.PeerListListener;
import wifidirect.p2pcore.callbackMessage;
import wifidirect.p2pcore.nodeP2pInfo;
import wifidirect.p2pcore.wifiP2pEvent;
import wifidirect.p2pcore.wifiP2pService;


public class WFDMesh implements EDProtocol, CDProtocol,
ConnectionInfoListener, DnsSdServiceResponseListener, DnsSdTxtRecordListener, PeerListListener, Callback,GroupInfoListener, 
BroadcastReceiver {


	public static final String 	SERVICE_REG_TYPE 		= "_presence._tcp"; // this is registeration type for services

	public static final int		BECOME_GO				= 0x400 + 13;		// Reserved Code for Message Handler
	public static final int		GO_REQUEST				= 0x400 + 14;		// Reserved Code for Message Handler
	public static final int		CLIENT_ADDRESS			= 0x400 + 15;		// Reserved Code for Message Handler

	public 	boolean 	isWifiP2pEnabled;	// would be set to true by Broadcast receiver when the wifi P2P enabled
	public 	boolean 	isConnected;    	// would be set to true by Broadcast receiver when the device state changed
	private boolean     isGroupFormed;
	private	int			groupID;
	private boolean		groupRecordUpdated;	//if group data needs to be updated
	private String		p2pMacAddress;
	public  String		goMacAddress;
	private int		connectionRequestTimer;	//if there is a GO_REQUEST message start countdown
	private int		requestDelayTimer;		//if there is another free node in sight wait this time before sending GO_REQUEST
	private double 	actionDelay;			//cooldown after performing an action
	private String firstGoRequestAddress;	//address of first device that requested to be a go

	

	// Object definitions
	private 		WifiP2pManager 				manager;
	private			WifiManager 				wifiManager;
	private 	 	WifiP2pConfig 				config; 			  		// configuration of wifi p2p. here for WPS configuration
	private			Group						newGroup;					// The current group if this device is GO. Otherwise it will return null
	private 		wifiP2pService			 	serviceGroup;
	private 		nodeP2pInfo					nodeInfo;

	// Collections									
	private ArrayList<WifiP2pDevice> 			peerList; 					// WifiP2pDevice of peers found in the proximity
	private List<WifiP2pDevice>					proximityGroupList;
	private HashMap<String, String> 			nsdRecords;					//list of key-value pairs broadcasted by nsd

	private Device								thisDevice;
	private long 								cycle, emptyGroupTimerReached;
	private int 								p2pInfoPid, wifip2pmanagerPid, wifimanagerPid;							
	
	public WFDMesh(String prefix) {
		p2pInfoPid 	 			= Configuration.getPid(prefix + "." + "p2pinfo");
		wifip2pmanagerPid  		= Configuration.getPid(prefix + "." + "p2pmanager");
		wifimanagerPid  		= Configuration.getPid(prefix + "." + "wifimanager");
		cycle 					= 0;
		emptyGroupTimerReached 	= 0;		
		isWifiP2pEnabled 		= false;	// would be set to true by Broadcast receiver when the wifi P2P enabled
		isGroupFormed			= false;
		groupID					= 0;
		groupRecordUpdated		= false;;
		p2pMacAddress			= "";
		goMacAddress			= "";
		proximityGroupList		= new ArrayList<WifiP2pDevice>();
		connectionRequestTimer	= -1;		//it changes to positive value when received GO_REQUEST message
		requestDelayTimer		= 0;
		actionDelay				= 0;
	}

	// This method will be called by Broadcast receiver when wifip2p state changed
	public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
		//            Toast.makeText(WFDMesh.this, "WiFiP2PEnabled = " + isWifiP2pEnabled, Toast.LENGTH_SHORT).show();
		this.isWifiP2pEnabled = isWifiP2pEnabled;
		if (isWifiP2pEnabled){
			appendStatus("WiFi P2P is enabled");
		}else{
			appendStatus("WiFi P2P is disabled");
		}
	}

	// This method will be called by Broadcast receiver when peer change occur
	public void setPeersChanged() {
		manager.requestPeers();
		manager.requestConnectionInfo();
	}

	//This method will be called by Broadcast receiver when connection status changed
	public void setConnectChanged(boolean connectionState){
		isConnected = connectionState;    	
		if(isConnected) {
			appendStatus("Connected!");
			isConnected = true;
		} else {
			appendStatus("Disconnected");
			changeDeviceType("free");
			isGroupFormed = false;
			groupRecordUpdated = false;
			isConnected = false;    			    			    			
		}
	}

	@Override
	public void nextCycle(Node node, int pid) {
		
		//thisNode = node;
		//thisPid = pid;
		if (cycle == 0){		//init


			// Initializing wifiP2P manager, Channel and Broadcast receiver and registering the receiver
			manager = (WifiP2pManager) node.getProtocol(wifip2pmanagerPid);
			wifiManager = (WifiManager) node.getProtocol(wifimanagerPid);
			nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);

			config 				= new WifiP2pConfig();  			// configuration of wifi p2p. here for WPS configuration
			newGroup			= new Group();						// The current group if this device is GO. Otherwise it will return null
			peerList 			= new ArrayList<WifiP2pDevice>(); 	// WifiP2pDevice of peers found in the proximity
			thisDevice 			= new Device(nodeInfo.getMacAddress(), "true", "free", null);		//TODO retrieve this wifip2pdevice
			nsdRecords			= new HashMap<String, String>();

			nsdRecords.put("type", thisDevice.deviceType);		//in the beginning
			nsdRecords.put("concurrent", thisDevice.concurrentMode);	//TODO randomize in the future
			nsdRecords.put("devicesN", "0");
			nsdRecords.put("clientsN", "0");


			// Register listeners. These are callbacks invoked
			// by the system when a service is actually discovered.
			manager.registerBroadcastReceiver(this);
			manager.registerPeerListener(this);
			manager.registerConInfoListener(this);
			manager.registerGroupInfoListener(this);
			manager.registerHandler(this);
			wifiManager.registerHandler(this);
			//wifiManager.startScan();

			// Configure the Intention and WPS in wifi P2P
			//Random r = new Random();

			// In this simulator the Intention value has been already fixed at the NodeP2pInfo class. We have not using the intention parameter in Configuration file in this simulator -- later I will fix this
			config.groupOwnerIntent = nodeInfo.getGoIntentionValue();
			// WPS in this simulator is always PBC -- it has been just modeled by a fix delay
			config.wps = "PBC";

			//p2pMacAddress = Utils.getMACAddress("p2p0");
			p2pMacAddress = nodeInfo.getMacAddress();
			// Calculating the intention and Putting the Intention of this device inside the intention List
			//intention = config.groupOwnerIntent;

			// Calling the first method in this activity
			//Visualizer.print("Cycle 1 on Node: " + node.getID() + " is finished");
			startRegistrationAndDiscovery();	//scenario 1
		}
		
		//////////////////////
		//	continuous part (not event driven, init and idle)
		//////////////////////

		if (actionDelay > 0) {		//device has started some action and it needs to wait some time before acting again
			actionDelay--;
		} else {
			switch (thisDevice.deviceType) {
			case "free":
				if (areThereGos()){		// if there are some groups around send request to join scenario 3
					String goAddress = chooseGo();
					appendStatus("Invitation sent to Group: " + goAddress);
					connectPeer(goAddress);			
				}
				if(areThereFreeNodes()) { 		// free nodes meet - scenario 2
					for (Device value : thisDevice.localNeighborList.values()) {
					    if (value.deviceType.equals("free")) {
					    	connectP2p(value.address);
					    	actionDelay = 3000/NodeMovement.CycleLenght;	//wait 3s
					    }
					}
				}
				break;
			case "go":
				// empty group time out check -- If a GO remains without any clients for 15 seconds then it should check around to see if it is possible to connect to another group as a client or not
				if(thisDevice.deviceType.equals("go") && thisDevice.clientsN==0 && emptyGroupTimerReached<=(5000/NodeMovement.CycleLenght)){
					emptyGroupTimerReached++;
				} else if(thisDevice.deviceType.equals("go") && thisDevice.clientsN==0 && emptyGroupTimerReached>(5000/NodeMovement.CycleLenght)){
					emptyGroupTimerReached = 0;
					manager.removeGroup();
					if (wifiManager.getWifiStatus()==Device.CONNECTED) {
						wifiManager.cancelConnect();
					}
					changeDeviceType("free");
					actionDelay = 5000/NodeMovement.CycleLenght;
					//if(!proximityGroupList.isEmpty()){
						//manager.removeGroup();
						// getting the first group in the list
						//config.deviceAddress = groupList.get((String) groupList.keySet().toArray()[0]);
						//config.deviceAddress = proximityGroupList.get(0).deviceAddress;
						//manager.connect(config);
					//}
				}else{
					emptyGroupTimerReached = 0;
				}
				if (connectionRequestTimer != -1) {		//go waits for more GO_REQUEST requests before sending a BECOME_GO message
					connectionRequestTimer++;
					if (connectionRequestTimer>=(15000/NodeMovement.CycleLenght)){	//scenario 5/scenario 6
						//send BECOME_GO message to first requesting device
						sendMessage(BECOME_GO);
						connectionRequestTimer = -1;
					}
				}
				break;
			case "client":
				//if device is already connected and sees another go - notifies a go by sending a BECOME_RELAY message
				if (areThereGos() || areThereFreeNodes()) {	//scenario 5/scenario 6	
					requestDelayTimer++;
				}
				if(requestDelayTimer>=(10000/NodeMovement.CycleLenght)) {	//wait 3s
					sendMessage(GO_REQUEST);
					requestDelayTimer = 0;
				}
				break;
			}
		}
		cycle++;
	}
	
	//returns the address of go with most clients
	public String chooseGo() {
		int maxClients = 0;
		int goCount = 0;
		WifiP2pDevice newGo = null;
		for (Entry<String, Device> device: thisDevice.localNeighborList.entrySet()) {
			if (device.getValue().deviceType.equals("go")) {
				newGo = device.getValue().p2pDevice;
				goCount++;
				if(device.getValue().clientsN > maxClients)
					maxClients = device.getValue().clientsN;
			}
		}
		if (goCount >= 2) {
			for (Entry<String, Device> device: thisDevice.localNeighborList.entrySet()) {
				if (maxClients == device.getValue().clientsN) {
					newGo = device.getValue().p2pDevice;
				}
			}
		}
		return newGo.deviceAddress;
	}
	
	public void sendMessage(int type) {
		callbackMessage newMessage = new callbackMessage();
		newMessage.what = type;
		newMessage.obj = p2pMacAddress;
		if (type == BECOME_GO) {
			manager.send(newMessage, firstGoRequestAddress);
		} else if (type == GO_REQUEST) {
			manager.send(newMessage, goMacAddress);
		} else if (type == CLIENT_ADDRESS) {
			manager.send(newMessage, goMacAddress);
		}
	}
	
	public WFDMesh clone(){
		WFDMesh wsda = null;
		try { wsda = (WFDMesh) super.clone(); }
		catch( CloneNotSupportedException e ) {} // never happens
		return wsda;
	}

	//checks localNeighborList for free nodes (so it can initiate connection)
	private boolean areThereFreeNodes() {
		boolean result = false;
		for (Device value : thisDevice.localNeighborList.values()) {
		    if (value.deviceType.equals("free")) {
		    	result = true;
		    }
		}
		//nsdRecords.put("Intention", String.valueOf(intention));	//intent val is auto
		//intentionList.put(p2pMacAddress, intention);
		return result;
	}
	
	private boolean areThereGos() {
		boolean result = false;
		for (Device value : thisDevice.localNeighborList.values()) {
		    if (value.deviceType.equals("go")) {
		    	result = true;
		    }
		}
		return result;
	}


	//Registers a local service and then initiates a service discovery    
	private void startRegistrationAndDiscovery() {	
		wifiP2pService service1 = new wifiP2pService("NSD", SERVICE_REG_TYPE, nsdRecords);
		manager.addLocalService(service1);
		// Start Service discovery
		manager.registerDnsSdResponseListeners(this);
		manager.registerDnsSdTxtRecordListener(this);
		// After attaching listeners, initiate discovery.
		manager.discoverServices();
	}

	@Override
	public void onDnsSdServiceAvailable(String instanceName,
			String registrationType, WifiP2pDevice srcDevice) {
		// A service has started broadcasting - ignore               
		
		WiFiP2pService service = new WiFiP2pService();

		service.device = srcDevice;			   // srcDevice is a WifiP2Device object
		service.instanceName = instanceName;   //instance name is the name of Service e.g. MAGNET
		service.serviceRegistrationType = registrationType;
	}

	//bonjour service available
	@Override
	public void onDnsSdTxtRecordAvailable(String fullDomainName, Map<String, String> record, WifiP2pDevice srcDevice) {
		//	react to new device in range
		Device discoveredDevice = new Device(srcDevice.deviceAddress, record.get("concurrent"), record.get("type"), srcDevice);
		thisDevice.localNeighborList.put(srcDevice.deviceAddress, discoveredDevice);				//put Device in a local list
		addDeviceToNsd(srcDevice.deviceAddress, false);		//TODO how to ensure consistency?
		startRegistrationAndDiscovery();
		if(discoveredDevice.deviceType.equals("go")) {
			thisDevice.localNeighborList.get(srcDevice.deviceAddress).clientsN = Integer.parseInt(record.get("clientsN"));
		}
		/*if(thisDevice.deviceType.equals("go") && discoveredDevice.deviceType.equals("go") && wifiManager.getWifiStatus()!=Device.CONNECTED) {		
			//go to first go connection scenario 7
			wifiManager.startScan();			//TODO this should work only once
		}*/
	}

	//public void connectP2p(WiFiP2pService service) {
	public void connectP2p(String address) {
		//Check to see whether this device has the highest Intention
		/*int maxIntention = 0;
		for(Entry<String, Device> device: localNeighborList.entrySet()) {
			if(device.getValue().deviceType.equals("free") && Integer.valueOf(device.getValue().devicesN)>maxIntention) {
				maxIntention = Integer.valueOf(device.getValue().devicesN);
			}
		}*/

		//if (thisDevice.clientsN == maxIntention){	//
		if (thisDevice.devicesN > thisDevice.localNeighborList.get(address).devicesN){	//if both are 0?
			//This device has the highest Intention
			appendStatus("This device has the highest Intention");
			//if (!isConnected && proximityGroupList.isEmpty()){   // IF this device is not connected and there is not any group around`	//TODO check for clients
			if (!isConnected && !areThereGos()){
				appendStatus("No Other groups; Creating a new group");
				createGroup();
			} 
				//wait for couple of seconds and then check to see if it is connected. otherwise creat a group
				//delayHandler1 = (long) (cycle+(calculateWait()/NodeMovement.CycleLenght));
				//delayHandler1Started = true;  	 
		}
		else {
			if(!isConnected && areThereGos()){
				for(WifiP2pDevice device: proximityGroupList){						
					connectPeer(device.deviceAddress);
					appendStatus("Invitation sent to Group: " + device.deviceAddress);
				}
			}			
			appendStatus("Not the highest Intention. Waiting to join the group");
		}
	}

	// Group Creation
	private void createGroup(){
		//appendStatus("Group List: " + groupList);
		// before advertising the new group first remove the previous one if there is any
		if (newGroup.getGroupName()!=null){ 
			// this means that we have already set a group name which means we already created a group which removed now
			// So we have to stop the group adv at service level by calling the following method
			// serviceGroup is created as a global field and set at the previous group generation
			//final String ID = String.valueOf(newGroup.getGroupID());
			manager.removeLocalService(serviceGroup);
			newGroup.resetGroup();
		}
		manager.createGroup();
		// A random Group number will be created
		if (groupID==0){
			groupID = (int)(Math.random() * 1000);
		}
		newGroup.setGroupID(groupID);       // Set Group ID
		newGroup.setGroupName("Group ID: " + String.valueOf(groupID));  // set Group name;

		appendStatus("Added Group " + String.valueOf(newGroup.getGroupID()));                    
		// If there are peers around, invite them to join the newly created group
		/*if (!peerList.isEmpty()){
			for (WifiP2pDevice device : peerList){
				if(device.status == Device.AVAILABLE){ // Send invitations if it is available (not connected not invited not unavailable)
					//appendStatus("Invitation sent to: " + device.deviceAddress); 	               		  		
					connectPeer(device.deviceAddress);
				}
			}						                   					         
		}*/
	}

	// get the device MAC address and connect to the peer
	private void connectPeer(String deviceMacAddress){
		config.deviceAddress = deviceMacAddress;
		appendStatus("Trying to connect to: " + config.deviceAddress);
		manager.connect(config);		  				
	}

	//everytime peers are updated
	@Override
	public void onPeersAvailable(WifiP2pDeviceList peers) {
		//Updating the Mac address List of all available devices around
		// We need this list sometimes to make computations easier
		// This list is as fresh as the peerList So better than the other list
		//only maintain and remove nsdRecords
		for(Entry<String, Device> device: thisDevice.localNeighborList.entrySet()) {
			if(peers.get(device.getKey()) == null) {
				removeDevicefromNsd(device.getKey(), false);
			}
		}
		/*for (WifiP2pDevice device: peerList){
			
			if (!thisDevice.localNeighborList.containsKey(device.deviceAddress)) {
				removeDevicefromNsd(device.deviceAddress, false);
			}
			if (device.isGroupOwner()){
				proximityGroupList.add(device);
			}
			//refresh nsd records
			//nsdRecords.remove("clientsN");
			//addDeviceToNsd(device.deviceAddress);
			//nsdRecords.put("device"+, value)
		}*/
		/*for(int i = 0; i < Integer.parseInt(nsdRecords.get("devicesN")); i++) {
			nsdRecords.remove("client"+Integer.toString(i));
		}*/
		/*nsdRecords.put("devicesN", "0");
		for(WifiP2pDevice device: peers.getDeviceList()) {
			addDeviceToNsd(device.deviceAddress, false);
		}*/
		startRegistrationAndDiscovery();
				// if we are the group owner we send the invitation to the newly found peer
		//if (isConnected && isGroupOwner){			
		/*if (nsdRecords.get("type").equals("go")){
			for (WifiP2pDevice device : peerList){
				if(device.status != Device.CONNECTED && device.status != Device.INVITED) {   // status = 3 means device available / status=0 means device connected / status = 1 means device invited					 
					connectPeer(device.deviceAddress);
				}
			}	
		}*/
	}

	// IF the device is connected, this method will be called
	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
		isGroupFormed = p2pInfo.groupFormed;

		// GO's and client's roles
		if (p2pInfo.isGroupOwner) {
			goMacAddress = p2pMacAddress;
			appendStatus("Connected as group owner");
			//nsdRecords.put( "clientsN", String.valueOf(Integer.parseInt(nsdRecords.get("clientsN")+1)) );	//TOdO dirty hax because ongroupinfo doesn't work properly
			//appendStatus("Group formed= " + isGroupFormed);
			manager.requestGroupInfo();   // will be informed at the onGroupInfoAvailable callback whenever it was ready
			/*if (nsdRecords.get("clientsN").equals("0")) {
				changeDeviceType("go");
				startRegistrationAndDiscovery();
			}*/
		} else if (isGroupFormed) {				// if it is not a GO, run a client socket handler
			//appendStatus("Group formed= " + p2pInfo.groupFormed);
			appendStatus("Connected as client to "+p2pInfo.groupOwnerAddress);
			goMacAddress = p2pInfo.groupOwnerAddress;
			changeDeviceType("client");
		}
	}
	
	//adds another device to a nsd record
	public void addDeviceToNsd(String address, boolean isClient) {

		if (isClient) {
			nsdRecords.put("client"+nsdRecords.get("clientsN"), address);
			thisDevice.clientsN = Integer.parseInt(nsdRecords.get("clientsN"))+1;
			nsdRecords.put( "clientsN", String.valueOf(thisDevice.clientsN) );
		} else {
			nsdRecords.put("device"+nsdRecords.get("devicesN"), address);
			thisDevice.devicesN = Integer.parseInt(nsdRecords.get("devicesN"))+1;
			nsdRecords.put("devicesN", String.valueOf(thisDevice.devicesN));
		}
	}
	
	//removes disconnected device from nsd record
	public void removeDevicefromNsd(String address, boolean isClient) {
		if (isClient) {
			boolean flag = false;
			for(int i = 0; i<Integer.valueOf(nsdRecords.get("clientsN")); i++) {
				if (nsdRecords.get("client"+String.valueOf(i)).equals(address)) {
					nsdRecords.remove("client"+nsdRecords.get(String.valueOf(i)), address);
					thisDevice.clientsN = Integer.parseInt(nsdRecords.get("clientsN"))-1;
					nsdRecords.put( "clientsN", String.valueOf(thisDevice.clientsN));
					flag = true;
					continue;
				}
				if(flag) {
					String tempAddress = nsdRecords.get("client"+nsdRecords.get(String.valueOf(i)));
					nsdRecords.remove("client"+nsdRecords.get(String.valueOf(i)));
					nsdRecords.put("client"+nsdRecords.get(String.valueOf(i-1)), tempAddress);
				}
			}	
		} else {
			boolean flag = false;
			for(int i = 0; i<Integer.valueOf(nsdRecords.get("devicesN")); i++) {
				if (nsdRecords.get("device"+String.valueOf(i)).equals(address)) {
					nsdRecords.remove("device"+nsdRecords.get(String.valueOf(i)), address);
					thisDevice.devicesN = Integer.parseInt(nsdRecords.get("devicesN"))-1;
					nsdRecords.put( "devicesN", String.valueOf(thisDevice.devicesN));
					flag = true;
					continue;
				}
				if(flag) {
					String tempAddress = nsdRecords.get("device"+nsdRecords.get(String.valueOf(i)));
					nsdRecords.remove("device"+nsdRecords.get(String.valueOf(i)));
					nsdRecords.put("device"+nsdRecords.get(String.valueOf(i-1)), tempAddress);
				}
			}
			//nsdRecords.put("device"+nsdRecords.get("devicesN"), address);
			//nsdRecords.put("devicesN", String.valueOf(Integer.parseInt(nsdRecords.get("devicesN"))+1));
		}
	}
	
	//changes device type to "go", "client" or "free" and changes nsd record
	public void changeDeviceType(String type) {
		if(type.equals("free")) {
			goMacAddress = null;
		}
		if(type.equals("go")) {
			nsdRecords.put("clientsN", "0");
			//nsdRecords.put("clientsN", String.valueOf(0));
		}
		thisDevice.deviceType = type;
		nsdRecords.put("type", thisDevice.deviceType);
	}

	// Status of the program which will be shown at the bottom
	public void appendStatus(String status) {
		Visualizer.print(String.valueOf(p2pMacAddress)+": " + status, Color.BLUE);
	}

	//Updating list of wifi APs			//don't think this is important
	/*public void updateWifiAPs(){
		//Visualizer.print("Update WIFI AP");
		List<ScanResult> wifiScanResults = new ArrayList<ScanResult>();
		wifiScanResults = wifiManager.getScanResults();
		for(ScanResult ap: wifiScanResults){
			appendStatus(ap.SSID);
		}
		if(thisDevice.deviceType.equals("go") && wifiManager.getWifiStatus()!=Device.CONNECTED) {
			//decideGroupConnection();			//TODO this should only be called after wifi scanning results are available <- check if this is working
		}
	}*/

	// Here Group Owner connects to another group owner via WiFi
	/*private void decideGroupConnection(){
		if (!(wifiManager.getWifiStatus()==Device.CONNECTED)) {
			appendStatus("decideGroupConnection");		
			WifiConfiguration wifiConfig = new WifiConfiguration();
			if(wifiManager.getScanResults().size()>0 && wifiManager.getScanResults().get(0)!=null){
				wifiConfig.SSID = wifiManager.getScanResults().get(0).SSID;
				wifiManager.connect(wifiConfig);
			}
		}
	}*/

	// ask clients to connect to the external groups via WiFi Interface		//not used anywhere		//now it's GO connecting to the other GO
	/*public void connectGroups(HashMap<String, String> bestSolution){
		if(isGroupOwner && isConnected){
			//TODO change conditions for getting links
			WifiConfiguration wifiConfig = new WifiConfiguration();
			wifiConfig.SSID = null;
			// find the relevant SSID for received BSSID (msg.obj)
			for(ScanResult apList: wifiManager.getScanResults()){
				if(apList.BSSID.equals(bestSolution.entrySet().iterator().next().getKey())) {
					//if(apList.BSSID.equals((String) msg.obj)){
					wifiConfig.SSID = apList.SSID;
				}
			}

			if(wifiConfig.SSID!=null){
				wifiManager.connect(wifiConfig);
			}
		}
	}*/

	@Override
	public void onReceive(wifiP2pEvent wifip2pevent) {
		String action = wifip2pevent.getEvent();

		if (action.equals("WIFI_P2P_STATE_CHANGED_ACTION")) {

			// UI update to indicate wifi p2p status.
			int state = manager.getExtraSystemInfo(WifiP2pManager.EXTRA_WIFI_STATE);
			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				// Wifi Direct mode is enabled
				setIsWifiP2pEnabled(true);
			} else {
				setIsWifiP2pEnabled(false);
			}
		}
		if (action.equals("WIFI_P2P_PEERS_CHANGED_ACTION")) {
			setPeersChanged();
		}
		if (action.equals("WIFI_P2P_CONNECTION_CHANGED_ACTION")) {
			if (manager == null) {
				return;
			}
			if (nodeInfo.getStatus()==Device.CONNECTED) {
				// we are connected with the other device, request connection
				// info to find group owner IP
				manager.requestConnectionInfo();
				setConnectChanged(true); // connected
			} else {
				setConnectChanged(false); // disconnect
			}
		} else if (action.equals("WIFI_P2P_THIS_DEVICE_CHANGED_ACTION")) {
			//WifiP2pDevice device = new WifiP2pDevice(thisNode, p2pInfoPid) ;
			//setDevicestatuschanged(device.status);
		}
		if (action.equals("SCAN_RESULTS_AVAILABLE_ACTION")) {
			//updateWifiAPs(); 
		}
		
	}

	//doesn't show connected nodes
	@Override
	public void onGroupInfoAvailable(WifiP2pGroup group) {
		if (newGroup.getGroupName()!=null && !groupRecordUpdated){	//new group
			newGroup.setGroupSSID(group.getSSID());
			newGroup.setGroupPassPhrase(group.getmPassphrase());
			manager.removeLocalService(serviceGroup);

			//TODO refresh nsd clients
			nsdRecords.put("ssid", newGroup.getGroupSSID());
			nsdRecords.put("password", newGroup.getGroupPassPhrase());
			changeDeviceType("go");
			//nsdRecords.put("clientsN", String.valueOf(group.getGroupSize()));		//can be changed by disappearance
			startRegistrationAndDiscovery();
			appendStatus("Group Record Updated " + String.valueOf(newGroup.getGroupID()));
			groupRecordUpdated = true;
		}
		
		for(int i = 0; i < Integer.parseInt(nsdRecords.get("clientsN")); i++) {
			//nsdRecords.remove("client"+String.valueOf(i));
			removeDevicefromNsd(nsdRecords.get("client"+String.valueOf(i)), true);
		}
		//nsdRecords.put("clientsN","0");
		// Add all peers found to the peerList
		for(Node cNode: group.getNodeList()){
			WifiP2pDevice newDevice = new WifiP2pDevice(cNode, p2pInfoPid);
			addDeviceToNsd(newDevice.deviceAddress, true);
		}
		/*for(Entry<String, Device> device: thisDevice.localNeighborList.entrySet()) {	//remove clients that left
			boolean flag = true;
			for(Node peer: group.getNodeList()) {
				WifiP2pDevice newDevice = new WifiP2pDevice(peer, p2pInfoPid);
				if(newDevice.deviceAddress == device.getKey()) 
					flag = false;
			}
			if(flag) {
				removeDevicefromNsd(device.getKey(), true);
			}
		}*/
		
		startRegistrationAndDiscovery();		
	}

	// when two devices (one group owner and one is client) connecting to each other the first message that is exchanging between them is MY_HANDLE message. Here we understand that the client is connected or the group owner has a new client
	@Override
	public void handleMessage(callbackMessage msg) {
		if(msg==null)
			return;
		switch (msg.what){
			case GO_REQUEST:
				//wait for other messages and set timer
				connectionRequestTimer = 0;
				firstGoRequestAddress = (String) msg.obj;	//TODO check if this is the address of sender
				appendStatus(msg.obj+" requested to become a GO");
				break;
			case BECOME_GO:
				// client disconnects from go, becomes a go
				manager.cancelConnect();
				createGroup();
				break;
		}
	}

	@Override
	public void processEvent(Node arg0, int arg1, Object arg2) {			// Auto-generated method stub
	}
}
