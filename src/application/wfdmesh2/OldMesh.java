package application.wfdmesh2;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import application.magnet3.WiFiP2pService;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import visualization.Visualizer;
import wifi.ScanResult;
import wifi.WifiManager;
import wifidirect.nodemovement.NodeMovement;
import wifidirect.p2pcore.BroadcastReceiver;
import wifidirect.p2pcore.Callback;
import wifidirect.p2pcore.WifiP2pConfig;
import wifidirect.p2pcore.WifiP2pDevice;
import wifidirect.p2pcore.WifiP2pDeviceList;
import wifidirect.p2pcore.WifiP2pGroup;
import wifidirect.p2pcore.WifiP2pInfo;
import wifidirect.p2pcore.WifiP2pManager;
import wifidirect.p2pcore.WifiP2pManager.ConnectionInfoListener;
import wifidirect.p2pcore.WifiP2pManager.DnsSdServiceResponseListener;
import wifidirect.p2pcore.WifiP2pManager.DnsSdTxtRecordListener;
import wifidirect.p2pcore.WifiP2pManager.GroupInfoListener;
import wifidirect.p2pcore.WifiP2pManager.PeerListListener;
import wifidirect.p2pcore.callbackMessage;
import wifidirect.p2pcore.nodeP2pInfo;
import wifidirect.p2pcore.wifiP2pEvent;
import wifidirect.p2pcore.wifiP2pService;


public class OldMesh implements EDProtocol, CDProtocol,
ConnectionInfoListener, DnsSdServiceResponseListener, DnsSdTxtRecordListener, PeerListListener, Callback,GroupInfoListener, 
BroadcastReceiver {


	public static final String 	SERVICE_REG_TYPE 		= "_presence._tcp"; // this is registeration type for services

	public static final int 	MESSAGE_READ 			= 0x400 + 1;		// Reserved Code for Message Handler 
	public static final int 	MY_HANDLE 				= 0x400 + 2;		// Reserved Code for Message Handler
	public static final int 	OBJECT_READ 			= 0x400 + 3;		// Reserved Code for Message Handler 
	public static final int     ALCON_APLIST_RESPONSE 	= 0x400 + 4;		// Reserved Code for Message Handler 
	public static final int     ALCON_APLIST_REQUEST	= 0x400 + 5;		// Reserved Code for Message Handler 
	public static final int		MESSAGE_TEST			= 0x400 + 6;		// Reserved Code for Message Handler 
	public static final int		REQUEST_AP_SEEN			= 0x400 + 7;        // Reserved Code for Message Handler 
	public static final int		CLIENT_AP_LIST			= 0x400 + 8;		// Reserved Code for Message Handler 
	public static final int		REQUEST_CONNECT_AP		= 0x400 + 9;		// Reserved Code for Message Handler
	public static final int		ROUTING_MESSAGE			= 0x400 + 10;		// Reserved Code for Message Handler
	public static final int		GO_SSID_RESPONSE		= 0x400 + 11;		// Reserved Code for Message Handler
	public static final int		REQUEST_SSID			= 0x400 + 12;		// Reserved Code for Message Handler
	public static final int		BECOME_GO				= 0x400 + 13;		// Reserved Code for Message Handler
	public static final int		GO_REQUEST				= 0x400 + 14;		// Reserved Code for Message Handler
	public static final int		CLIENT_ADDRESS			= 0x400 + 15;		// Reserved Code for Message Handler


	public static final int 	waitcoeff 				= 300;				// Waiting time will be multiply by this coefficient (milliseconds)
	public static final int 	SERVER_PORT 			= 4545;				// Server port is the same in GroupOwnerSocketHandler
	public String AbstractState = "";

	// None constant variables
	public 	int 		count;
	public 	int 		groupCapacity;		// will be incremented by one for each new peer in the group
	public 	boolean 	discoveryFlag;	
	public 	boolean 	isWifiP2pEnabled;	// would be set to true by Broadcast receiver when the wifi P2P enabled
	public 	boolean 	isGroupOwner;		// would be set to true by ConnectionInfoListener when connection info avilable
	public 	boolean 	isConnected;    	// would be set to true by Broadcast receiver when the device state changed
	public 	double   	intention;			// The intention of this device. we may set it once to save memory instead of calling getIntention() method several times
	private boolean     isGroupFormed;
	private	int			groupID;
	private boolean		groupRecordUpdated;	//if group data needs to be updated
	private String		p2pMacAddress;
	public  String		goMacAddress;
	private int		connectionRequestTimer;	//if there is a GO_REQUEST message start countdown
	private int		requestDelayTimer;		//if there is another free node in sight wait this time before sending GO_REQUEST
	private String firstGoRequestAddress;	//address of first device that requested to be a go

	

	// Object definitions
	private 		WifiP2pManager 				manager;
	private			WifiManager 				wifiManager;
	private 	 	WifiP2pConfig 				config; 			  		// configuration of wifi p2p. here for WPS configuration
	private			Group						newGroup;					// The current group if this device is GO. Otherwise it will return null
	private 		wifiP2pService			 	serviceGroup;
	private 		nodeP2pInfo					nodeInfo;

	// Collections									
	private HashMap<String, Double> 			intentionList;  			// Intention	<=>	Device Mac Address
	private HashMap<String, Device> 			localNeighborList;  			// Device info of discovered devices
	//private HashMap<String, String> 			serviceList;  				// Device Mac Address <=> Service Name
	private ArrayList<WifiP2pDevice> 			peerList; 					// WifiP2pDevice of peers found in the proximity
	//private ArrayList<WifiP2pDevice> 			groupedPeerList; 			// WifiP2pDevice of peers in the group  
	private ArrayList<String>					macAddressList;
	private List<WifiP2pDevice>					proximityGroupList;
	private HashMap<String, String> 			nsdRecords;					//list of key-value pairs broadcasted by nsd

	private Device								thisDevice;
	private long 								cycle, emptyGroupTimerReached;
	private int 								p2pInfoPid, wifip2pmanagerPid, wifimanagerPid, transport0Pid, /*thisPid,*/ routaodvPid;							
	
	public OldMesh(String prefix) {
		p2pInfoPid 	 			= Configuration.getPid(prefix + "." + "p2pinfo");
		wifip2pmanagerPid  		= Configuration.getPid(prefix + "." + "p2pmanager");
		wifimanagerPid  		= Configuration.getPid(prefix + "." + "wifimanager");
		transport0Pid			= Configuration.getPid(prefix + "." + "transport0");
		routaodvPid				= Configuration.getPid(prefix + "." + "routaodv");
		cycle 					= 0;
		emptyGroupTimerReached 	= 0;		
		count 					= 0;
		groupCapacity 			= 0;		// will be incremented by one for each new peer in the group
		discoveryFlag 			= false;	
		isWifiP2pEnabled 		= false;	// would be set to true by Broadcast receiver when the wifi P2P enabled
		isGroupOwner 			= false;	// would be set to true by ConnectionInfoListener when connection info available
		isConnected 			= false;    // would be set to true by Broadcast receiver when the device state changed
		intention 				= 0;		// The intention of this device. we may set it once to save memory instead of calling getIntention() method several times
		isGroupFormed			= false;
		groupID					= 0;
		groupRecordUpdated		= false;;
		p2pMacAddress			= "";
		goMacAddress			= "";
		proximityGroupList		= new ArrayList<WifiP2pDevice>();
		connectionRequestTimer	= -1;		//it changes to positive value when received GO_REQUEST message
		requestDelayTimer		= 0;
	}

	// This method will be called by Broadcast receiver when wifip2p state changed
	public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
		//            Toast.makeText(WFDMesh.this, "WiFiP2PEnabled = " + isWifiP2pEnabled, Toast.LENGTH_SHORT).show();
		this.isWifiP2pEnabled = isWifiP2pEnabled;
		if (isWifiP2pEnabled){
			appendStatus("WiFi P2P is enabled");
		}else{
			appendStatus("WiFi P2P is disabled");
		}
	}

	// This method will be called by Broadcast receiver when peer change occur
	public void setPeersChanged() {
		updatePeerList();
	}

	//This method will be called by Broadcast receiver when connection status changed
	public void setConnectChanged(boolean connectionState){
		isConnected = connectionState;    	
		if(isConnected) {
			appendStatus("Connected!");
			isConnected = true;
		} else {
			appendStatus("Disconnected");
			isGroupOwner = false;
			isGroupFormed = false;
			groupRecordUpdated = false;
			isConnected = false;    			    			    			
		}
	}

	@Override
	public void nextCycle(Node node, int pid) {
		
		//thisNode = node;
		//thisPid = pid;
		if (cycle == 0){		//init


			// Initializing wifiP2P manager, Channel and Broadcast receiver and registering the receiver
			manager = (WifiP2pManager) node.getProtocol(wifip2pmanagerPid);
			wifiManager = (WifiManager) node.getProtocol(wifimanagerPid);
			nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);

			//handler 			= new Handler(this);
			config 				= new WifiP2pConfig();  			// configuration of wifi p2p. here for WPS configuration
			newGroup			= new Group();						// The current group if this device is GO. Otherwise it will return null
			intentionList 		= new HashMap<String, Double>();  	// Intention	<=>	Device Mac Address
			localNeighborList 	= new HashMap<String, Device>();  	// List of discovered devices
			//serviceList 		= new HashMap<String, String>();  	// Device Mac Address <=> Service Name
			//groupList 			= new HashMap<String, String>();  	// Group ID   <=> Mac Address of GO
			peerList 			= new ArrayList<WifiP2pDevice>(); 	// WifiP2pDevice of peers found in the proximity
			//groupedPeerList 	= new ArrayList<WifiP2pDevice>(); 	// WifiP2pDevice of peers in the group  
			//serverThreads		= new ArrayList<GroupOwnerSocketHandler>(); // each client need a seprate thread and socket
			//chatClientList		= new ArrayList<ChatManager>();  	 // List of ChatManagers. A chatManager Object is needed for each client to send message
			//chatClientList		= new ArrayList<Node>(); 
			macAddressList  	= new ArrayList<String>();
			//WTAList 			= new ArrayList<WTAClass>();
			thisDevice 			= new Device(nodeInfo.getMacAddress(), "true", "free", null);		//TODO retrieve this wifip2pdevice
			nsdRecords			= new HashMap<String, String>();

			nsdRecords.put("Intention", String.valueOf(intention));	//intent val is auto
			nsdRecords.put("type", thisDevice.deviceType);		//in the beginning
			nsdRecords.put("concurrent", thisDevice.concurrentMode);	//TODO randomize in the future
			nsdRecords.put("noDevices", "0");
			nsdRecords.put("clientsN", "0");


			// Register listeners. These are callbacks invoked
			// by the system when a service is actually discovered.
			manager.registerBroadcastReceiver(this);
			manager.registerPeerListener(this);
			manager.registerConInfoListener(this);
			manager.registerGroupInfoListener(this);
			manager.registerHandler(this);
			wifiManager.registerHandler(this);
			//wifiManager.startScan();

			// Configure the Intention and WPS in wifi P2P
			//Random r = new Random();

			// In this simulator the Intention value has been already fixed at the NodeP2pInfo class. We have not using the intention parameter in Configuration file in this simulator -- later I will fix this
			config.groupOwnerIntent = nodeInfo.getGoIntentionValue();
			// WPS in this simulator is always PBC -- it has been just modeled by a fix delay
			config.wps = "PBC";

			//p2pMacAddress = Utils.getMACAddress("p2p0");
			p2pMacAddress = nodeInfo.getMacAddress();
			// Calculating the intention and Putting the Intention of this device inside the intention List
			//intention = config.groupOwnerIntent;
			intentionList.put(p2pMacAddress, intention);

			// Calling the first method in this activity
			//Visualizer.print("Cycle 1 on Node: " + node.getID() + " is finished");
			startRegistrationAndDiscovery();	//scenario 1
		}
		
		
		//////////////////////
		//	continuous part (not event driven, init and idle)
		//////////////////////

		// empty group time out check -- If a GO remains without any clients for 15 seconds then it should check around to see if it is possible to connect to another group as a client or not
		// If it is possible the GO will remove its group and will connect to the other group as client
		if(isGroupOwner && thisDevice.clientsN==0 && emptyGroupTimerReached<=(15000/NodeMovement.CycleLenght)){		//TODO groupedPeerList doesn't work the first time
			emptyGroupTimerReached++;
		} else if(isGroupOwner && thisDevice.clientsN==0 && emptyGroupTimerReached>(15000/NodeMovement.CycleLenght)){
			emptyGroupTimerReached = 0;
			manager.removeGroup();
			if (wifiManager.getWifiStatus()==Device.CONNECTED) {
				wifiManager.cancelConnect();
			}
			changeDeviceType("free");
			//if(!proximityGroupList.isEmpty()){
				//manager.removeGroup();
				// getting the first group in the list
				//config.deviceAddress = groupList.get((String) groupList.keySet().toArray()[0]);
				//config.deviceAddress = proximityGroupList.get(0).deviceAddress;
				//manager.connect(config);
			//}
		}else{
			emptyGroupTimerReached = 0;
		}
			
		//if (!proximityGroupList.isEmpty() && thisDevice.deviceType.equals("free")){      // if there are some groups around send request to join scenario 3
		if (areThereGos() && thisDevice.deviceType.equals("free")){
			int minClients = 8;
			/*WifiP2pDevice newGo = proximityGroupList.iterator().next();
			//for(WifiP2pDevice device: proximityGroupList){
			for (Device device : localNeighborList.values()) {		//find which go has least clients
				if (device.clientsN < minClients && device.deviceType.equals("go")) {
					minClients = device.clientsN;
					newGo = device.p2pDevice;
				}
			}*/
			int goCount = 0;
			WifiP2pDevice newGo = null;
			for (Entry<String, Device> device: localNeighborList.entrySet()) {
				if (device.getValue().deviceType.equals("go")) {
					newGo = device.getValue().p2pDevice;
					goCount++;
				}
			}
			appendStatus("Invitation sent to Group: " + newGo.deviceAddress);
			connectPeer(newGo.deviceAddress);
			//delayHandler3 = (long) (cycle + (calculateWait()/NodeMovement.CycleLenght));
			//delayHandler3Started = true;				
		}
		
		if(thisDevice.deviceType.equals("free") && areThereFreeNodes()) { //scenario 2
			WiFiP2pService newService = new WiFiP2pService();	//TODO whateva
			connectP2p(newService);
		}
		
		//if device is already connected and sees another go - notifies a go by sending a BECOME_RELAY message
		if (thisDevice.deviceType.equals("client") && (areThereGos() || areThereFreeNodes())) {	//scenario 5/scenario 6	
			requestDelayTimer++;
		}
		if(thisDevice.deviceType.equals("client") && requestDelayTimer>=(3000/NodeMovement.CycleLenght)) {	//wait 3s
			sendMessage(GO_REQUEST);
			requestDelayTimer = 0;
		}
		
		//go waits for more GO_REQUEST requests before sending a
		if (connectionRequestTimer != -1) {
			connectionRequestTimer++;
			if (connectionRequestTimer>=(5000/NodeMovement.CycleLenght)){	//scenario 5/scenario 6
				//send BECOME_GO message to first requesting device
				sendMessage(BECOME_GO);
				connectionRequestTimer = -1;
			}
		}
		
		cycle++;
	}
	
	public void sendMessage(int type) {
		callbackMessage newMessage = new callbackMessage();
		newMessage.what = type;
		newMessage.obj = p2pMacAddress;
		if (type == BECOME_GO) {
			manager.send(newMessage, firstGoRequestAddress);
		} else if (type == GO_REQUEST) {
			manager.send(newMessage, goMacAddress);
		} else if (type == CLIENT_ADDRESS) {
			manager.send(newMessage, goMacAddress);
		}
	}
	
	public OldMesh clone(){
		OldMesh wsda = null;
		try { wsda = (OldMesh) super.clone(); }
		catch( CloneNotSupportedException e ) {} // never happens
		wsda.p2pInfoPid 			= p2pInfoPid;
		wsda.wifip2pmanagerPid 		= wifip2pmanagerPid;
		wsda.wifiManager 			= wifiManager;
		wsda.transport0Pid 			= transport0Pid;
		wsda.routaodvPid			= routaodvPid;
		//wsda.thisPid				= 0;
		wsda.cycle 					= 0;
		wsda.emptyGroupTimerReached = 0;
		wsda.count				  	= 0;
		wsda.groupCapacity		  	= 0;
		wsda.discoveryFlag		  	= false;
		wsda.isWifiP2pEnabled 		= false;	// would be set to true by Broadcast receiver when the wifi P2P enabled
		wsda.isGroupOwner 			= false;	// would be set to true by ConnectionInfoListener when connection info avilable
		wsda.isConnected 			= false;    // would be set to true by Broadcast receiver when the device state changed
		wsda.intention 				= 0;		// The intention of this device. we may set it once to save memory instead of calling getIntention() method several times
		wsda.isGroupFormed			= false;
		wsda.groupID				= 0;
		wsda.groupRecordUpdated		= false;
		wsda.p2pMacAddress			= "";
		wsda.goMacAddress			= "";
		wsda.proximityGroupList		= new ArrayList<WifiP2pDevice>();
		return wsda;
	}

	//checks localNeighborList for free nodes (so it can initiate connection)
	private boolean areThereFreeNodes() {
		intention = 0;
		boolean result = false;
		for (Device value : localNeighborList.values()) {
		    if (value.deviceType.equals("free")) {
		    	result = true;
		    	intention++;		//TODO this should be in device discovery
		    }
		}
		//nsdRecords.put("Intention", String.valueOf(intention));	//intent val is auto
		//intentionList.put(p2pMacAddress, intention);
		return result;
	}
	
	private boolean areThereGos() {
		boolean result = false;
		for (Device value : localNeighborList.values()) {
		    if (value.deviceType.equals("go")) {
		    	result = true;
		    }
		}
		return result;
	}


	//Registers a local service and then initiates a service discovery    
	private void startRegistrationAndDiscovery() {	
		wifiP2pService service1 = new wifiP2pService("NSD", SERVICE_REG_TYPE, nsdRecords);
		manager.addLocalService(service1);
		// Start Service discovery
		manager.registerDnsSdResponseListeners(this);
		manager.registerDnsSdTxtRecordListener(this);
		// After attaching listeners, initiate discovery.
		manager.discoverServices();
	}

	@Override
	public void onDnsSdServiceAvailable(String instanceName,
			String registrationType, WifiP2pDevice srcDevice) {
		// A service has started broadcasting - ignore               
		
		WiFiP2pService service = new WiFiP2pService();

		service.device = srcDevice;			   // srcDevice is a WifiP2Device object
		service.instanceName = instanceName;   //instance name is the name of Service e.g. MAGNET
		service.serviceRegistrationType = registrationType;

		// putting the new service inside the serviceList only if it has not already been there.
		//if(!(serviceList.containsKey(srcDevice.deviceAddress) && serviceList.containsValue(instanceName))){
		//	serviceList.put(srcDevice.deviceAddress, instanceName);
		//}
	}

	//bonjour service available
	@Override
	public void onDnsSdTxtRecordAvailable(String fullDomainName, Map<String, String> record, WifiP2pDevice srcDevice) {
		//	react to new device in range
		//appendStatus(String.valueOf(srcDevice.deviceName + " " + record.get("type")));
		Device discoveredDevice = new Device(srcDevice.deviceAddress, record.get("concurrent"), record.get("type"), srcDevice);
		localNeighborList.put(srcDevice.deviceAddress, discoveredDevice);				//put Device in a local list
		//addDeviceToNsd(address);
		
		//nsdRecords.put("discoveredDevice"+nsdRecords.get("noDevices"), srcDevice.deviceAddress);
		//nsdRecords.put("noDevices", String.valueOf(Integer.parseInt(nsdRecords.get("noDevices"))+1));
		if(discoveredDevice.deviceType.equals("go")) {
			localNeighborList.get(srcDevice.deviceAddress).clientsN = Integer.parseInt(record.get("clientsN"));
		}
		if(thisDevice.deviceType.equals("go") && discoveredDevice.deviceType.equals("go") && wifiManager.getWifiStatus()!=Device.CONNECTED) {		
			//go to first go connection scenario 7
			wifiManager.startScan();			//TODO this should work only once
		}
		
		// Put this new Record in the intention List
		//intentionList.put(srcDevice.deviceAddress, Double.parseDouble(record.get("Intention")));
		intentionList.put(srcDevice.deviceAddress, Double.parseDouble(record.get("noDevices")));
	}


	// This method actually will be called when the related view has been clicked on the fragment. 
	// But to make the click autonomous, I have also start this method after couple of seconds by passing "MAGNET" to it
	// which means I mimic the click scenario
	public void connectP2p(WiFiP2pService service) {
	//public void connectP2p(String address) {

		//Finding the maximum intention inside the intentionList HashMap
		String deviceMaxIntention = null;
		double maxValueInMap=(Collections.max(intentionList.values())); // This will return max value in the Hashmap
		//for (Map.Entry<String, Double> entry : intentionList.entrySet()) {  // Iterate through hashmap
		for (Map.Entry<String, Double> entry : intentionList.entrySet()) {  // Iterate through hashmap
			if (entry.getValue()==maxValueInMap) {
				deviceMaxIntention = entry.getKey();     				// find the device name with the max intention
			}
		}

		//Check to see whether this device has the highest Intention
		//If this device has the highest intention it will create a group
		//else it will wait for the other device to create a group. if no groups found it will create one

		if (p2pMacAddress.equals(deviceMaxIntention)){
			//This device has the highest Intention
			appendStatus("This device has the highest Intention");
			//if (!isConnected && proximityGroupList.isEmpty()){   // IF this device is not connected and there is not any group around`	//TODO check for clients
			if (!isConnected && !areThereGos()){
				appendStatus("No Other groups; Creating a new group");
				createGroup();
			//} else if(!isConnected && !proximityGroupList.isEmpty()){
			} else if(!isConnected && areThereGos()){
				for(WifiP2pDevice device: proximityGroupList){						
					connectPeer(device.deviceAddress);
					appendStatus("Invitation sent to Group: " + device.deviceAddress);
				}
				//wait for couple of seconds and then check to see if it is connected. otherwise creat a group
				//delayHandler1 = (long) (cycle+(calculateWait()/NodeMovement.CycleLenght));
				//delayHandler1Started = true;
			}  	 
		}
		else { 
			//Not the highest Intention. It will wait and then check the group list for any 
			// available groups and try to connect to the group owner of each group.
			
			appendStatus("Not the highest Intention");
			//appendStatus("will wait for " + calculateWait() + " miliseconds"); 
			//delayHandler2 = (long)(cycle + (calculateWait()/NodeMovement.CycleLenght));
			//delayHandler2Started = true;    
		}
	}

	// Group Creation
	private void createGroup(){
		//appendStatus("Group List: " + groupList);
		// before advertising the new group first remove the previous one if there is any
		if (newGroup.getGroupName()!=null){ 
			// this means that we have already set a group name which means we already created a group which removed now
			// So we have to stop the group adv at service level by calling the following method
			// serviceGroup is created as a global field and set at the previous group generation
			//final String ID = String.valueOf(newGroup.getGroupID());
			manager.removeLocalService(serviceGroup);
			newGroup.resetGroup();
		}
		manager.createGroup();
		// A random Group number will be created
		if (groupID==0){
			groupID = (int)(Math.random() * 1000);
		}
		newGroup.setGroupID(groupID);       // Set Group ID
		newGroup.setGroupName("Group ID: " + String.valueOf(groupID));  // set Group name;

		appendStatus("Added Group " + String.valueOf(newGroup.getGroupID()));                    
		// If there are peers around, invite them to join the newly created group
		if (!peerList.isEmpty()){
			for (WifiP2pDevice device : peerList){
				if(device.status == Device.AVAILABLE){ // Send invitations if it is available (not connected not invited not unavailable)
					//appendStatus("Invitation sent to: " + device.deviceAddress); 	               		  		
					connectPeer(device.deviceAddress);
				}
			}						                   					         
		}
	}

	// get the device MAC address and connect to the peer
	private void connectPeer(String deviceMacAddress){
		config.deviceAddress = deviceMacAddress;
		appendStatus("Trying to connect to: " + config.deviceAddress);
		manager.connect(config);		  				
	}

	@Override
	public void onPeersAvailable(WifiP2pDeviceList peers) {

		//ArrayList<WifiP2pDevice> tempPeer = new ArrayList<WifiP2pDevice>();
		peerList.clear();   // Clear All elements because the peer changed may be caused by peer disappearance
		peerList.addAll(peers.getDeviceList());	 // Add all peers found to the peerList

		//Updating the Mac address List of all available devices around
		// We need this list sometimes to make computations easier
		// This list is as fresh as the peerList So better than the other list
		//TODO change nsd records
		for(int i = 0; i < Integer.parseInt(nsdRecords.get("noDevices")); i++) {
			nsdRecords.remove("client"+Integer.toString(i));
		}
		nsdRecords.put("noDevices", "0");
		for(WifiP2pDevice device: peers.getDeviceList()) {
			addDeviceToNsd(device.deviceAddress, false);
		}
		startRegistrationAndDiscovery();
		macAddressList.clear();
		proximityGroupList.clear();
		for (WifiP2pDevice device: peerList){
			macAddressList.add(device.deviceAddress);
			if (device.isGroupOwner()){
				proximityGroupList.add(device);
			}
			//refresh nsd records
			//nsdRecords.remove("clientsN");
			//addDeviceToNsd(device.deviceAddress);
			//nsdRecords.put("discoveredDevice"+, value)
		}
		// Updating Intention List and remove all intentions from devices that are not available anymore
		for (Iterator<Entry<String, Double>> itr = intentionList.entrySet().iterator(); itr.hasNext();)
		{
			Map.Entry<String, Double> entrySet = (Entry<String, Double>) itr.next();
			String Key = entrySet.getKey();
			if (!macAddressList.contains((Key)))
			{
				itr.remove();
			}
		}
		intentionList.put(p2pMacAddress, intention);  // adding the Intention of this device again because it was removed at the above procedure

		// if we are the group owner we send the invitation to the newly found peer
		//if (isConnected && isGroupOwner){			
		if (nsdRecords.get("type").equals("go")){
			for (WifiP2pDevice device : peerList){
				if(device.status != Device.CONNECTED && device.status != Device.INVITED) {   // status = 3 means device available / status=0 means device connected / status = 1 means device invited					 
					connectPeer(device.deviceAddress);
				}
			}	
		}
	}

	// Update Peer List
	public void  updatePeerList(){
		// updating peer list
		manager.requestPeers();
		manager.requestConnectionInfo();
	}

	// IF the device is connected, this method will be called
	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
		isGroupFormed = p2pInfo.groupFormed;
		/*
		 * The group owner accepts connections using a server socket and then spawns a
		 * client socket for every client. This is handled by {@code
		 * GroupOwnerSocketHandler}
		 */
		// GO's and client's roles
		if (p2pInfo.isGroupOwner) {
			isGroupOwner = true;
			goMacAddress = p2pMacAddress;		//TODO add to nsd
			appendStatus("Connected as group owner");
			//appendStatus("Group formed= " + isGroupFormed);
			manager.requestGroupInfo();   // will be infomed at the onGroupInfoAvailable callback whenever it was ready
			if (nsdRecords.get("clientsN").equals("0")) {
				changeDeviceType("go");
				//nsdRecords.put("clientsN", "0");
				startRegistrationAndDiscovery();
			}// else {
				//TODO get client's address and add it through addDeviceToNsd
				//nsdRecords.put("clientsN", String.valueOf(Integer.parseInt(nsdRecords.get("clientsN"))+1));		//TODO overlap  with addDeviceToNsd
			//}
			//nsdRecords.put("client"+getClientsNumber(), p2pInfo.);	//TODO get new client's address and number
			//addDeviceToNsd(p2pInfo.address);
			
		} else if (isGroupFormed) {				// if it is not a GO, run a client socket handler
			//appendStatus("Group formed= " + p2pInfo.groupFormed);
			appendStatus("Connected as client to "+p2pInfo.groupOwnerAddress);
			isGroupOwner = false;
			goMacAddress = p2pInfo.groupOwnerAddress;
			changeDeviceType("client");
			
			//send a message to GO informing about your address
			sendMessage(CLIENT_ADDRESS);
		}
	}
	
	//adds another device to a nsd record
	public void addDeviceToNsd(String address, boolean isClient) {
		nsdRecords.put("discoveredDevice"+nsdRecords.get("noDevices"), address);
		nsdRecords.put("noDevices", String.valueOf(Integer.parseInt(nsdRecords.get("noDevices"))+1));
		if (isClient) {
			nsdRecords.put("client"+nsdRecords.get("clientsN"), address);
			nsdRecords.put( "clientsN", String.valueOf(Integer.parseInt(nsdRecords.get("clientsN")+1)) );
		}
	}
	
	//changes device type to "go", "client" or "free" and changes nsd record
	public void changeDeviceType(String type) {
		if(type.equals("free")) {
			goMacAddress = null;
		}
		if(type.equals("go")) {
			nsdRecords.put("clientsN", "0");
			//nsdRecords.put("clientsN", String.valueOf(0));
		}
		thisDevice.deviceType = type;
		nsdRecords.put("type", thisDevice.deviceType);
	}

	// Status of the program which will be shown at the bottom
	public void appendStatus(String status) {
		Visualizer.print(String.valueOf(p2pMacAddress)+": " + status, Color.BLUE);
	}

	// Clearing the service requests and start a fresh discovery
	// discovery stops after connection but we need to continue discovery
	public void restartServiceDiscovery(){	//TODO make changes to nsd
		//wifiManager.startScan();
		manager.stopPeerDiscovery();
		manager.discoverServices();
		//appendStatus("Service discovery initiated");                
	}

	//Updating list of wifi APs
	public void updateWifiAPs(){
		//Visualizer.print("Update WIFI AP");
		List<ScanResult> wifiScanResults = new ArrayList<ScanResult>();
		wifiScanResults = wifiManager.getScanResults();
		for(ScanResult ap: wifiScanResults){
			//appendStatus(ap.SSID);
		}
		if(thisDevice.deviceType.equals("go") && wifiManager.getWifiStatus()!=Device.CONNECTED) {
			
			//decideGroupConnection();			//TODO this should only be called after wifi scanning results are available <- check if this is working
		}
	}

	// Calculate Waiting time for being second or ... Intention dynamically based on Intention
	private int calculateWait(){
		return (int) ((-5*intention) + 75)*waitcoeff; // it will return waiting time Intention=14 => wait 5s, Inten = 1 => wait 70s
	}

	// Here Group Owner connects to another group owner via WiFi
	/*private void decideGroupConnection(){
		if (!(wifiManager.getWifiStatus()==Device.CONNECTED)) {
			appendStatus("decideGroupConnection");		
			WifiConfiguration wifiConfig = new WifiConfiguration();
			if(wifiManager.getScanResults().size()>0 && wifiManager.getScanResults().get(0)!=null){
				wifiConfig.SSID = wifiManager.getScanResults().get(0).SSID;
				wifiManager.connect(wifiConfig);
			}
		}
	}*/

	// ask clients to connect to the external groups via WiFi Interface		//not used anywhere		//now it's GO connecting to the other GO
	/*public void connectGroups(HashMap<String, String> bestSolution){
		if(isGroupOwner && isConnected){
			//TODO change conditions for getting links
			WifiConfiguration wifiConfig = new WifiConfiguration();
			wifiConfig.SSID = null;
			// find the relevant SSID for received BSSID (msg.obj)
			for(ScanResult apList: wifiManager.getScanResults()){
				if(apList.BSSID.equals(bestSolution.entrySet().iterator().next().getKey())) {
					//if(apList.BSSID.equals((String) msg.obj)){
					wifiConfig.SSID = apList.SSID;
				}
			}

			if(wifiConfig.SSID!=null){
				wifiManager.connect(wifiConfig);
			}
		}
	}*/

	@Override
	public void onReceive(wifiP2pEvent wifip2pevent) {
		String action = wifip2pevent.getEvent();

		if (action.equals("WIFI_P2P_STATE_CHANGED_ACTION")) {

			// UI update to indicate wifi p2p status.
			int state = manager.getExtraSystemInfo(WifiP2pManager.EXTRA_WIFI_STATE);
			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				// Wifi Direct mode is enabled
				setIsWifiP2pEnabled(true);
			} else {
				setIsWifiP2pEnabled(false);
			}
		}
		if (action.equals("WIFI_P2P_PEERS_CHANGED_ACTION")) {
			setPeersChanged();
		}
		if (action.equals("WIFI_P2P_CONNECTION_CHANGED_ACTION")) {
			if (manager == null) {
				return;
			}
			if (nodeInfo.getStatus()==Device.CONNECTED) {
				// we are connected with the other device, request connection
				// info to find group owner IP
				manager.requestConnectionInfo();
				setConnectChanged(true); // connected
			} else {
				setConnectChanged(false); // disconnect
				changeDeviceType("free");
				// It's a disconnect
			}
		} else if (action.equals("WIFI_P2P_THIS_DEVICE_CHANGED_ACTION")) {
			//WifiP2pDevice device = new WifiP2pDevice(thisNode, p2pInfoPid) ;
			//setDevicestatuschanged(device.status);
		}
		if (action.equals("SCAN_RESULTS_AVAILABLE_ACTION")) {
			updateWifiAPs(); 
		}
		
	}

	//doesn't show connected nodes
	@Override
	public void onGroupInfoAvailable(WifiP2pGroup group) {

		//groupedPeerList.clear();   // Clear All elements because the peer changed may be caused by peer disappearance
		for(int i = 0; i < Integer.parseInt(nsdRecords.get("clientsN")); i++) {
			nsdRecords.remove("client"+String.valueOf(i));
		}
		nsdRecords.put("clientsN","0");
		// Add all peers found to the peerList 
		for(Node cNode: group.getNodeList()){
			WifiP2pDevice newDevice = new WifiP2pDevice(cNode, p2pInfoPid);
			//groupedPeerList.add(newDevice);
			//TODO add client to nsd
			addDeviceToNsd(newDevice.deviceAddress, true);
		}
		startRegistrationAndDiscovery();

		//appendStatus("Number of devices in the group: " + (groupedPeerList.size()+1)); 
		//appendStatus("Group SSID: " + group.getSSID());
		//appendStatus("Group PASS: " + group.getmPassphrase());
		if (newGroup.getGroupName()!=null && !groupRecordUpdated){
			newGroup.setGroupSSID(group.getSSID());
			newGroup.setGroupPassPhrase(group.getmPassphrase());
			manager.removeLocalService(serviceGroup);

			//TODO refresh nsd clients
			nsdRecords.put("ssid", newGroup.getGroupSSID());
			nsdRecords.put("password", newGroup.getGroupPassPhrase());
			changeDeviceType("go");
			//nsdRecords.put("clientsN", String.valueOf(group.getGroupSize()));		//can be changed by disappearance
			startRegistrationAndDiscovery();
			appendStatus("Group Record Updated " + String.valueOf(newGroup.getGroupID()));
			groupRecordUpdated = true;
		}
	}

	// when two devices (one group owner and one is client) connecting to each other the first message that is exchanging between them is MY_HANDLE message. Here we understand that the client is connected or the group owner has a new client
	@Override
	public void handleMessage(callbackMessage msg) {
		if(msg==null)
			return;
		switch (msg.what){
			case GO_REQUEST:
				//wait for other messages and set timer
				connectionRequestTimer = 0;
				firstGoRequestAddress = (String) msg.obj;	//TODO check if this is the address of sender
				appendStatus(msg.obj+" requested to become a GO");
				break;
			case BECOME_GO:
				// client disconnects from go, becomes a go
				manager.cancelConnect();
				createGroup();
				break;
			case CLIENT_ADDRESS:	//idk if this is necessary if group info returns all clients anyway
				//device connected - maybe add to some list?
				//addDeviceToNsd((String) msg.obj, true);
				break;
		}
	}

	@Override
	public void processEvent(Node arg0, int arg1, Object arg2) {			// Auto-generated method stub
	}
}