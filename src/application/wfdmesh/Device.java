package application.wfdmesh;

import wifidirect.p2pcore.WifiP2pDevice;

public class Device {
	
	public String deviceType;
	public String concurrentMode;
	public String address;
	public int clientsN;
	public int devicesN;
	public WifiP2pDevice p2pDevice;
	
	public Device(String address, String concurrentMode, String deviceType, WifiP2pDevice p2pDevice) {
		this.deviceType = deviceType;
		this.concurrentMode = concurrentMode;
		this.address = address;
		this.clientsN = 0;
		this.devicesN = 0;
		this.p2pDevice = p2pDevice;
	}

}
