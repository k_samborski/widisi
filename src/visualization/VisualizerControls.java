package visualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import applications.magnet.Routing.AodvStats;
import nodemovement.NodeMovement;
import peersim.config.Configuration;

/**
 * 
 */
public class VisualizerControls extends JPanel {
	private static final long serialVersionUID = 1L;

	private JTextField realTime;
	private JTextField simTime;
	private JTextField netSize;
	private JTextField numGroups;
	private JTextField maxNodeSpeed;
	private JTextField minNodeSpeed;
	private JTextField numConnectedNodes;
	private JTextField numclients;
	private JTextField lastCycleTimeMs;
	private JTextField currentCycle;
	private JTextField timeOutInfo;
	private JTextField maxNeighbor;
	private JTextField avShortestPath;
	private JTextField shortestPathCount;
	private JTextField connectivityTextField;
	private JTextField avgConnectivityTextField;
	private JTextField ruleOneCheck;
	private JTextField ruleTwoCheck;
	private JTextField ruleThreeCheck;
	private JTextField ruleFourCheck;
	private JTextField ruleFiveCheck;
	private JTextField ruleSixCheck;
	private JTextField ruleSevenCheck;
	private JTextField ruleEightCheck;
	private JTextField ruleNineCheck;
	private JTextField ruleTenCheck;
	private JTextField ruleElevenCheck;
	private JTextField ruleTwelveCheck;
	private JTextField cycleLengthControl;
	private JTextField fieldLengthControl;
	private JTextField maxSpeedControl;
	private JTextField minSpeedControl;
	private JTextField radioRangeControl;
	private JTextField maxClientControl;
	private JTextField maxNetSizeControl;
	private JTextField minNetSizeControl;
	private JTextField aodvPacketsSend;
	private JTextField rreqPacketsSend;
	private JTextField rerrPacketsSend;
	private JTextField ravaPacketsSend;
	private JTextField rrepPacketsSend;
	private JTextField helloPacketsSend;
	private JTextField rsucPackets;
	private JTextField netMsgsPackets;
	private JTextField rreqOrigPackets;
	private JTextField aodvPacketsReceive;
	private JTextField helloPacketsReceive;
	private JTextField rreqPacketsReceive;
	private JTextField rrepPacketsReceive;
	private JTextField rerrPacketsReceive;
	private JTextField ravaPacketsReceive;
	private JTextField rrepOrigPackets;
	private JTextField singleNodeMovTextField;

	public JRadioButton rdbtnExpand;
	private Visualizer visualizer;

	private JPanel panelNetworkInfo;

	private JPanel panelWFDViolationMonitor;

	private JPanel panelRoutingStatistics;

	private JPanel panelNetworkControl;

	private JRadioButton rbNoMove;

	private JRadioButton rbMoveAll;

	private JRadioButton rbMoveThisNode;

	private JTextField cycleLengthNetms;

	private JTextField fieldLengthInfo;

	private JTextField maxNetNodeSpeedInfo;

	private JTextField minNetNodeSpeedInfo;

	private JTextField radioRangeInfo;

	private JTextField maxClientInfo;

	private JTextField maxNetSizeInfo;

	private JTextField minNetSizeInfo;

	private int MaxNetSIZE;

	private int MinNetSIZE;

	private int WFDGroupCapacity;

	private JButton btnStepSimulator;

	private JButton btnStepUntilSimulator;

	private JButton btnRunUntilSimulator;

	private JButton btnResumeSimulator;

	private JButton btnPauseSimulator;

	private JTextField tvStepRunUntilCycle;

	private JPanel panelSimulatorControl;

	/**
	 * Create controls panel
	 * 
	 * @param fieldlength
	 * @param radioRangeLength
	 * @param radioRangeLength
	 * 
	 */
	public VisualizerControls(Visualizer visualizer, int fieldlength, int radioRangeLength) {
		this.visualizer = visualizer;

		// AT: I tried with BoxLayout, but conln't remove panels
		setLayout(new BorderLayout());

		add(getNetworkInfo(), BorderLayout.NORTH);

		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(getNetworkControl(), BorderLayout.NORTH);
		add(p1, BorderLayout.CENTER);

		JPanel p2 = new JPanel(new BorderLayout());
		p2.add(getPanelSimulationControl(), BorderLayout.NORTH);
		p1.add(p2, BorderLayout.CENTER);

		JPanel p3 = new JPanel(new BorderLayout());
		p3.add(getWFDViolationMonitor(), BorderLayout.NORTH);
		p2.add(p3, BorderLayout.CENTER);

		JPanel p4 = new JPanel(new BorderLayout());
		p4.add(getRoutingStatistics(), BorderLayout.NORTH);
		p3.add(p4, BorderLayout.CENTER);

		MaxNetSIZE = Configuration.getInt("MAX_NET_SIZE");
		MinNetSIZE = Configuration.getInt("MIN_NET_SIZE");
		WFDGroupCapacity = Configuration.getInt("WFD_GROUP_CAPACITY");

		initValues(fieldlength, radioRangeLength);

	}

	/**
	 * @param fieldlength
	 * @param radioRangeLength
	 * @param radioRangeLength
	 * 
	 */
	public void initValues(int fieldlength, int radioRangeLength) {
		cycleLengthControl.setText(String.valueOf(NodeMovement.CycleLenght));
		// fieldLengthControl.setText("--");
		// radioRangeControl.setText(String.valueOf(NodeMovement.radio));
		maxSpeedControl.setText(String.valueOf(NodeMovement.SpeedMx));
		minSpeedControl.setText(String.valueOf(NodeMovement.SpeedMn));
		maxClientControl.setText(String.valueOf(WFDGroupCapacity));

		cycleLengthNetms.setText(String.valueOf(NodeMovement.CycleLenght));
		fieldLengthInfo.setText(String.valueOf(fieldlength));
		radioRangeInfo.setText(String.valueOf(radioRangeLength));

		maxNetNodeSpeedInfo.setText(String.valueOf(NodeMovement.SpeedMx));
		minNetNodeSpeedInfo.setText(String.valueOf(NodeMovement.SpeedMn));
		maxClientInfo.setText(String.valueOf(WFDGroupCapacity));

		maxNetSizeInfo.setText(String.valueOf(MaxNetSIZE));
		minNetSizeInfo.setText(String.valueOf(MinNetSIZE));
	}

	/**
	 * 
	 */
	public void showPanelWFDRules(boolean show) {
		getWFDViolationMonitor().setVisible(show);
	}

	/**
	 * 
	 */
	public void showPanelRoutingStatistics(boolean show) {
		getRoutingStatistics().setVisible(show);
	}

	/**
	 * 
	 */
	public void showPanelNetworkControl(boolean show) {
		getNetworkControl().setVisible(show);
	}

	/**
	 * 
	 */
	private JPanel getPanelSimulationControl() {

		if (panelSimulatorControl != null)
			return panelSimulatorControl;

		panelSimulatorControl = new JPanel();

		TitledBorder tl = new TitledBorder("Simulator Control");
		tl.setTitleJustification(TitledBorder.CENTER);
		tl.setTitleColor(Color.RED);
		panelSimulatorControl.setBorder(tl);

		btnPauseSimulator = new JButton("Pause");
		panelSimulatorControl.add(btnPauseSimulator);

		btnResumeSimulator = new JButton("Res");
		btnResumeSimulator.setVisible(false);
		btnResumeSimulator.setToolTipText("Resume Simulator");
		panelSimulatorControl.add(btnResumeSimulator);

		btnStepSimulator = new JButton("Step");
		panelSimulatorControl.add(btnStepSimulator);
		btnStepSimulator.setVisible(false);

		btnStepUntilSimulator = new JButton("StepU");
		panelSimulatorControl.add(btnStepUntilSimulator);
		btnStepUntilSimulator.setVisible(false);
		btnStepUntilSimulator.setToolTipText("Step Until Simulator");

		btnRunUntilSimulator = new JButton("RunU");
		panelSimulatorControl.add(btnRunUntilSimulator);
		btnRunUntilSimulator.setVisible(false);
		btnRunUntilSimulator.setToolTipText("Run Until Simulator");

		tvStepRunUntilCycle = new JTextField(4);
		panelSimulatorControl.add(tvStepRunUntilCycle);
		tvStepRunUntilCycle.setVisible(false);

		ActionListener al = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JButton btn = (JButton) e.getSource();
				switch (btn.getText()) {
				case "Pause":
					visualizer.pauseSimulator();
					btnPauseSimulator.setVisible(false);
					btnResumeSimulator.setVisible(true);
					btnStepSimulator.setVisible(true);
					btnStepUntilSimulator.setVisible(true);
					btnRunUntilSimulator.setVisible(true);
					tvStepRunUntilCycle.setVisible(true);
					break;
				case "Res":
					visualizer.resumeSimulator();
					btnPauseSimulator.setVisible(true);
					btnResumeSimulator.setVisible(false);
					btnStepSimulator.setVisible(false);
					btnStepUntilSimulator.setVisible(false);
					btnRunUntilSimulator.setVisible(false);
					tvStepRunUntilCycle.setVisible(false);
					break;
				case "Step":
					visualizer.stepSimulator(false);
					break;
				case "StepU":
					int stepToCycle = Integer.parseInt(tvStepRunUntilCycle.getText());
					if (stepToCycle > visualizer.getCurrentCycle()) {
						visualizer.setStepUntilSimulator(stepToCycle);
						setEnableStepRunUntilButtons(false);
					} else
						Visualizer.print("Step to a non valid cycle number...");
					break;
				case "RunU":
					int runToCycle = Integer.parseInt(tvStepRunUntilCycle.getText());
					if (runToCycle > visualizer.getCurrentCycle()) {
						visualizer.setRunUntilSimulator(runToCycle);
						setEnableStepRunUntilButtons(false);
					} else
						Visualizer.print("Run to a non valid cycle number...");
					break;
				}
			}
		};

		btnPauseSimulator.addActionListener(al);
		btnResumeSimulator.addActionListener(al);
		btnStepSimulator.addActionListener(al);
		btnStepUntilSimulator.addActionListener(al);
		btnRunUntilSimulator.addActionListener(al);
		return panelSimulatorControl;
	}

	/**
	 * 
	 */
	public void setEnableStepRunUntilButtons(boolean enable) {
		btnResumeSimulator.setEnabled(enable);
		btnStepSimulator.setEnabled(enable);
		btnStepUntilSimulator.setEnabled(enable);
		btnRunUntilSimulator.setEnabled(enable);
		tvStepRunUntilCycle.setEnabled(enable);
	}

	/**
	 * 
	 */
	private JPanel getNetworkInfo() {
		if (panelNetworkInfo != null)
			return panelNetworkInfo;

		int nInputColumns = 4;

		panelNetworkInfo = new JPanel(new GridBagLayout());

		GridConstraints cons = new GridConstraints(2, 12, 2);

		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(2, 2, 2, 2);
		cons.anchor = GridBagConstraints.WEST;

		TitledBorder tl = new TitledBorder("Network Info");
		tl.setTitleJustification(TitledBorder.CENTER);
		tl.setTitleColor(Color.RED);
		panelNetworkInfo.setBorder(tl);

		// Col 0

		JLabel lblFieldLenghtm = new JLabel("Field Lenght (m)");
		panelNetworkInfo.add(lblFieldLenghtm, cons.setNextConstraintsWithValues());

		(fieldLengthInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(fieldLengthInfo, cons.setNextConstraintsWithValues());

		JLabel lblRadioRangem = new JLabel("Radio Range (m)");
		panelNetworkInfo.add(lblRadioRangem, cons.setNextConstraintsWithValues());

		(radioRangeInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(radioRangeInfo, cons.setNextConstraintsWithValues());

		JLabel lblCurrentRealTime = new JLabel("Current Real Time (S)");
		panelNetworkInfo.add(lblCurrentRealTime, cons.setNextConstraintsWithValues());

		realTime = new JTextField(nInputColumns);
		realTime.setEditable(false);
		panelNetworkInfo.add(realTime, cons.setNextConstraintsWithValues());

		JLabel lblSimulatorTimes = new JLabel("Simulator Time (S)");
		panelNetworkInfo.add(lblSimulatorTimes, cons.setNextConstraintsWithValues());

		simTime = new JTextField(nInputColumns);
		simTime.setEditable(false);
		panelNetworkInfo.add(simTime, cons.setNextConstraintsWithValues());

		// XXX

		JLabel lblCurrentPeersimCycle = new JLabel("Current Cycle");
		panelNetworkInfo.add(lblCurrentPeersimCycle, cons.setNextConstraintsWithValues());

		(currentCycle = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(currentCycle, cons.setNextConstraintsWithValues());

		JLabel lblCurCycleTimeMs = new JLabel("Last Cyc time (ms)");
		panelNetworkInfo.add(lblCurCycleTimeMs, cons.setNextConstraintsWithValues());

		(lastCycleTimeMs = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(lastCycleTimeMs, cons.setNextConstraintsWithValues());

		JLabel lblCycleLenghtms = new JLabel("Cycle Lenght (ms)");
		lblCycleLenghtms.setToolTipText("Metwork Cycle Lenght (ms)");
		panelNetworkInfo.add(lblCycleLenghtms, cons.setNextConstraintsWithValues());

		(cycleLengthNetms = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(cycleLengthNetms, cons.setNextConstraintsWithValues());

		JLabel lblNetworkSizenodes = new JLabel("Net Size (Nodes)");
		panelNetworkInfo.add(lblNetworkSizenodes, cons.setNextConstraintsWithValues());

		(netSize = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(netSize, cons.setNextConstraintsWithValues());

		JLabel lblMaxNetworkSize = new JLabel("Net Max Size (nodes)");
		panelNetworkInfo.add(lblMaxNetworkSize, cons.setNextConstraintsWithValues());

		(maxNetSizeInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(maxNetSizeInfo, cons.setNextConstraintsWithValues());

		JLabel lblMinNetworkSize = new JLabel("Net Min Size (nodes)");
		panelNetworkInfo.add(lblMinNetworkSize, cons.setNextConstraintsWithValues());

		(minNetSizeInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(minNetSizeInfo, cons.setNextConstraintsWithValues());

		JLabel lblNetMaxNodeSpeed = new JLabel("Net Max Speed (m/s)");
		lblNetMaxNodeSpeed.setToolTipText("Net Max Node Speed (m/s)");
		panelNetworkInfo.add(lblNetMaxNodeSpeed, cons.setNextConstraintsWithValues());

		(maxNetNodeSpeedInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(maxNetNodeSpeedInfo, cons.setNextConstraintsWithValues());

		JLabel lblNetMinNodeSpeed = new JLabel("Net Min Speed (m/s)");
		lblNetMinNodeSpeed.setToolTipText("Net Min Node Speed (m/s)");
		panelNetworkInfo.add(lblNetMinNodeSpeed, cons.setNextConstraintsWithValues());

		(minNetNodeSpeedInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(minNetNodeSpeedInfo, cons.setNextConstraintsWithValues());

		// Col 1

		JLabel lblMaxNoOf_2 = new JLabel("Max Net GO Clients");
		lblMaxNoOf_2.setToolTipText("Maximum Number of Clients per WiFi Direct group");
		panelNetworkInfo.add(lblMaxNoOf_2, cons.setNextConstraintsWithValues());

		(maxClientInfo = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(maxClientInfo, cons.setNextConstraintsWithValues());

		JLabel lblNodeMaxNeighbours = new JLabel("Max Neighbours");
		panelNetworkInfo.add(lblNodeMaxNeighbours, cons.setNextConstraintsWithValues());

		maxNeighbor = new JTextField(nInputColumns);
		maxNeighbor.setEditable(false);
		panelNetworkInfo.add(maxNeighbor, cons.setNextConstraintsWithValues());

		JLabel lblMaxNoOf = new JLabel("Max Clients/Group");
		panelNetworkInfo.add(lblMaxNoOf, cons.setNextConstraintsWithValues());

		numclients = new JTextField(nInputColumns);
		numclients.setEditable(false);
		panelNetworkInfo.add(numclients, cons.setNextConstraintsWithValues());

		JLabel lblNoOfGroups = new JLabel("# Groups");
		panelNetworkInfo.add(lblNoOfGroups, cons.setNextConstraintsWithValues());

		numGroups = new JTextField(nInputColumns);
		numGroups.setEditable(false);
		panelNetworkInfo.add(numGroups, cons.setNextConstraintsWithValues());

		JLabel lblNoOfConnected = new JLabel("Avg. Connected (%)");
		panelNetworkInfo.add(lblNoOfConnected, cons.setNextConstraintsWithValues());

		numConnectedNodes = new JTextField(nInputColumns);
		numConnectedNodes.setEditable(false);
		panelNetworkInfo.add(numConnectedNodes, cons.setNextConstraintsWithValues());

		JLabel lblNoOfTimeout = new JLabel("# of TimeOut");
		panelNetworkInfo.add(lblNoOfTimeout, cons.setNextConstraintsWithValues());

		timeOutInfo = new JTextField("0", nInputColumns);
		timeOutInfo.setEditable(false);
		panelNetworkInfo.add(timeOutInfo, cons.setNextConstraintsWithValues());

		JLabel lblFree = new JLabel("Av Shortest Path");
		panelNetworkInfo.add(lblFree, cons.setNextConstraintsWithValues());

		avShortestPath = new JTextField(nInputColumns);
		avShortestPath.setEditable(false);
		panelNetworkInfo.add(avShortestPath, cons.setNextConstraintsWithValues());

		JLabel lblFree_1 = new JLabel("Sh Path Count");
		panelNetworkInfo.add(lblFree_1, cons.setNextConstraintsWithValues());

		shortestPathCount = new JTextField(nInputColumns);
		shortestPathCount.setEditable(false);
		panelNetworkInfo.add(shortestPathCount, cons.setNextConstraintsWithValues());

		JLabel lblNa = new JLabel("Connectivity (%)");
		panelNetworkInfo.add(lblNa, cons.setNextConstraintsWithValues());

		(connectivityTextField = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(connectivityTextField, cons.setNextConstraintsWithValues());

		JLabel lblNa_1 = new JLabel("Con Average (%)");
		panelNetworkInfo.add(lblNa_1, cons.setNextConstraintsWithValues());

		(avgConnectivityTextField = new JTextField(nInputColumns)).setEditable(false);
		panelNetworkInfo.add(avgConnectivityTextField, cons.setNextConstraintsWithValues());

		JLabel lblMaxNodeSpeed = new JLabel("Cur Max Node (m/s)");
		lblMaxNodeSpeed.setToolTipText("Cur Max Node Speed (m/s)");
		panelNetworkInfo.add(lblMaxNodeSpeed, cons.setNextConstraintsWithValues());

		maxNodeSpeed = new JTextField(nInputColumns);
		maxNodeSpeed.setEditable(false);
		panelNetworkInfo.add(maxNodeSpeed, cons.setNextConstraintsWithValues());

		JLabel lblMinNodeSpeed = new JLabel("Cur Min Speed (m/s)");
		lblMinNodeSpeed.setToolTipText("Cur Min Node Speed (m/s)");
		panelNetworkInfo.add(lblMinNodeSpeed, cons.setNextConstraintsWithValues());

		minNodeSpeed = new JTextField(nInputColumns);
		minNodeSpeed.setEditable(false);
		panelNetworkInfo.add(minNodeSpeed, cons.setNextConstraintsWithValues());

		return panelNetworkInfo;
	}

	/**
	 * 
	 */
	private JPanel getNetworkControl() {
		if (panelNetworkControl != null)
			return panelNetworkControl;

		int nInputColumns = 4;

		panelNetworkControl = new JPanel(new GridBagLayout());

		GridConstraints cons = new GridConstraints(2, 5, 2);
		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(2, 2, 2, 2);
		cons.anchor = GridBagConstraints.WEST;

		TitledBorder tl = new TitledBorder("Network Control");
		tl.setTitleJustification(TitledBorder.CENTER);
		tl.setTitleColor(Color.RED);
		panelNetworkControl.setBorder(tl);

		// =================================================

		JLabel lblCycleLenghtms = new JLabel("Cycle Lenght (ms)");
		panelNetworkControl.add(lblCycleLenghtms, cons.setNextConstraintsWithValues());

		cycleLengthControl = new JTextField(nInputColumns);
		panelNetworkControl.add(cycleLengthControl, cons.setNextConstraintsWithValues());

		JLabel lblFieldLenghtm = new JLabel("Field Lenght (m)");
		panelNetworkControl.add(lblFieldLenghtm, cons.setNextConstraintsWithValues());

		fieldLengthControl = new JTextField(nInputColumns);
		panelNetworkControl.add(fieldLengthControl, cons.setNextConstraintsWithValues());

		JLabel lblMaxNodeSpeed_1 = new JLabel("Max Speed (m/s)");
		panelNetworkControl.add(lblMaxNodeSpeed_1, cons.setNextConstraintsWithValues());

		maxSpeedControl = new JTextField(nInputColumns);
		panelNetworkControl.add(maxSpeedControl, cons.setNextConstraintsWithValues());

		JLabel lblMinNodeSpeed_1 = new JLabel("Min Speed (m/s)");
		panelNetworkControl.add(lblMinNodeSpeed_1, cons.setNextConstraintsWithValues());

		minSpeedControl = new JTextField(nInputColumns);
		panelNetworkControl.add(minSpeedControl, cons.setNextConstraintsWithValues());

		cons.setNextConstraintsWithValues();
		cons.setNextConstraintsWithValues();

		// =================================================
		JLabel lblRadioRangem = new JLabel("Radio Range (m)");
		panelNetworkControl.add(lblRadioRangem, cons.setNextConstraintsWithValues());

		radioRangeControl = new JTextField(nInputColumns);
		panelNetworkControl.add(radioRangeControl, cons.setNextConstraintsWithValues());

		JLabel lblMaxNoOf_2 = new JLabel("Max # Clients");
		lblMaxNoOf_2.setToolTipText("Maximum Number of Clients per WiFi Direct group");
		panelNetworkControl.add(lblMaxNoOf_2, cons.setNextConstraintsWithValues());

		maxClientControl = new JTextField(nInputColumns);
		panelNetworkControl.add(maxClientControl, cons.setNextConstraintsWithValues());

		JLabel lblMaxNetworkSize = new JLabel("Max Net Size");
		panelNetworkControl.add(lblMaxNetworkSize, cons.setNextConstraintsWithValues());

		maxNetSizeControl = new JTextField(nInputColumns);
		panelNetworkControl.add(maxNetSizeControl, cons.setNextConstraintsWithValues());

		JLabel lblMinNetworkSize = new JLabel("Min Net Size");
		panelNetworkControl.add(lblMinNetworkSize, cons.setNextConstraintsWithValues());

		minNetSizeControl = new JTextField(nInputColumns);
		panelNetworkControl.add(minNetSizeControl, cons.setNextConstraintsWithValues());

		JPanel panelButtonsLoadSave = new JPanel();
		JButton btnLoad = new JButton("Load");
		panelButtonsLoadSave.add(btnLoad);
		JButton btnSet = new JButton("Set");
		panelButtonsLoadSave.add(btnSet);
		cons.gridwidth = 2;
		cons.anchor = GridBagConstraints.CENTER;
		panelNetworkControl.add(panelButtonsLoadSave, cons.setNextConstraintsWithValues());
		cons.gridwidth = 1;

		// last row
		cons.gridx = 0;
		cons.gridy = 5;
		cons.gridwidth = 4;
		cons.anchor = GridBagConstraints.CENTER;
		JPanel panelMoveNodes = new JPanel();
		// panelMoveNodes.setBorder(BorderFactory.createLineBorder(Color.gray));
		JLabel lblMoveNodes = new JLabel("Move nodes: ");
		panelMoveNodes.add(lblMoveNodes);

		ButtonGroup bgMoveNodes = new ButtonGroup();
		rbNoMove = new JRadioButton("No");
		panelMoveNodes.add(rbNoMove);
		bgMoveNodes.add(rbNoMove);
		rbNoMove.setSelected(true);
		rbMoveAll = new JRadioButton("All");
		panelMoveNodes.add(rbMoveAll);
		bgMoveNodes.add(rbMoveAll);
		rbMoveThisNode = new JRadioButton("This:");
		panelMoveNodes.add(rbMoveThisNode);
		bgMoveNodes.add(rbMoveThisNode);
		singleNodeMovTextField = new JTextField(nInputColumns);
		singleNodeMovTextField.setToolTipText("Enter the ID of the node to move");
		panelMoveNodes.add(singleNodeMovTextField);
		panelNetworkControl.add(panelMoveNodes, cons);

		// listeners
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cycleLengthControl.setText(String.valueOf(NodeMovement.CycleLenght));
				// fieldLengthControl.setText("---" dg dfg);
				// .setText(String.valueOf(NodeMovement.radio));
				maxSpeedControl.setText(String.valueOf(NodeMovement.SpeedMx));
				minSpeedControl.setText(String.valueOf(NodeMovement.SpeedMn));
				// vizProxyBut.setSelected(Visualizer.isShowNetworkImage());
				singleNodeMovTextField.setText(null);
				rbNoMove.setSelected(true);

			}
		});

		btnSet.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				visualizer.connectivityAvg = 0;
				visualizer.connValNum = 0;
				NodeMovement.CycleLenght = Double.parseDouble(cycleLengthControl.getText());
				// NodeMovement.FieldLength =
				// Double.parseDouble(fieldLengthControl.getText());
				// NodeMovement.radio =
				// Double.parseDouble(radioRangeControl.getText());
				NodeMovement.SpeedMx = Double.parseDouble(maxSpeedControl.getText());
				NodeMovement.SpeedMn = Double.parseDouble(minSpeedControl.getText());
				// TODO
				NodeMovement.anyNodeSel = rbMoveAll.isSelected();
				NodeMovement.singleNodeSel = rbMoveThisNode.isSelected();
				if (rbMoveThisNode.isSelected()) {
					NodeMovement.singleNodeId = Long.parseLong(singleNodeMovTextField.getText());
				}

				JOptionPane.showMessageDialog(null, "New Seetings Set!");
			}
		});

		return panelNetworkControl;
	}

	/**
	 * 
	 */
	private JPanel getWFDViolationMonitor() {
		if (panelWFDViolationMonitor != null)
			return panelWFDViolationMonitor;

		panelWFDViolationMonitor = new JPanel(new GridBagLayout());

		int nInputColumns = 2;

		GridConstraints cons = new GridConstraints(6, 2, 2);
		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(2, 2, 2, 2);
		cons.anchor = GridBagConstraints.WEST;

		TitledBorder tl = new TitledBorder("WiFi Direct Violation Monitor - WFD Rules");
		tl.setTitleJustification(TitledBorder.CENTER);
		tl.setTitleColor(Color.RED);
		panelWFDViolationMonitor.setBorder(tl);

		JLabel lblRule = new JLabel("R 1");
		lblRule.setToolTipText("A peer cannot connect to another peer outside its proximity range");
		panelWFDViolationMonitor.add(lblRule, cons.setNextConstraintsWithValues());

		ruleOneCheck = new JTextField("0", nInputColumns);
		ruleOneCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleOneCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_1 = new JLabel("R 2");
		lblRule_1.setToolTipText("A group owner cannot connect to another group");
		panelWFDViolationMonitor.add(lblRule_1, cons.setNextConstraintsWithValues());

		ruleTwoCheck = new JTextField("0", nInputColumns);
		ruleTwoCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleTwoCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_2 = new JLabel("R 3");
		lblRule_2.setToolTipText("This Node is a Client of another device which is not Group Owner");
		panelWFDViolationMonitor.add(lblRule_2, cons.setNextConstraintsWithValues());

		ruleThreeCheck = new JTextField("0", nInputColumns);
		ruleThreeCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleThreeCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_3 = new JLabel("R 4");
		lblRule_3.setToolTipText("A peer cannot see more than X number of devices and services");
		panelWFDViolationMonitor.add(lblRule_3, cons.setNextConstraintsWithValues());

		ruleFourCheck = new JTextField("0", nInputColumns);
		ruleFourCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleFourCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_4 = new JLabel("R 5");
		panelWFDViolationMonitor.add(lblRule_4, cons.setNextConstraintsWithValues());

		ruleFiveCheck = new JTextField("0", nInputColumns);
		ruleFiveCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleFiveCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_5 = new JLabel("R 6");
		lblRule_5.setToolTipText(
				"A peer cannot discover other peers or services if they have not started peer discovery");
		panelWFDViolationMonitor.add(lblRule_5, cons.setNextConstraintsWithValues());

		ruleSixCheck = new JTextField("0", nInputColumns);
		ruleSixCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleSixCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_6 = new JLabel("R 7");
		lblRule_6.setToolTipText("A peer cannot be group owner and client at the same time");
		panelWFDViolationMonitor.add(lblRule_6, cons.setNextConstraintsWithValues());

		ruleSevenCheck = new JTextField("0", nInputColumns);
		ruleSevenCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleSevenCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_7 = new JLabel("R 8");
		lblRule_7.setToolTipText("A group cannot consist of more than M peers");
		panelWFDViolationMonitor.add(lblRule_7, cons.setNextConstraintsWithValues());

		ruleEightCheck = new JTextField("0", nInputColumns);
		ruleEightCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleEightCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_8 = new JLabel("R 9");
		panelWFDViolationMonitor.add(lblRule_8, cons.setNextConstraintsWithValues());

		ruleNineCheck = new JTextField("0", nInputColumns);
		ruleNineCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleNineCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_9 = new JLabel("R 10");
		lblRule_9.setToolTipText(
				"A Client cannot communicate with other client in the same group directly (bypassing GO)");
		panelWFDViolationMonitor.add(lblRule_9, cons.setNextConstraintsWithValues());

		ruleTenCheck = new JTextField("0", nInputColumns);
		ruleTenCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleTenCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_10 = new JLabel("R 11");
		panelWFDViolationMonitor.add(lblRule_10, cons.setNextConstraintsWithValues());

		ruleElevenCheck = new JTextField("0", nInputColumns);
		ruleElevenCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleElevenCheck, cons.setNextConstraintsWithValues());

		JLabel lblRule_11 = new JLabel("R 12");
		panelWFDViolationMonitor.add(lblRule_11, cons.setNextConstraintsWithValues());

		ruleTwelveCheck = new JTextField("0", nInputColumns);
		ruleTwelveCheck.setEditable(false);
		panelWFDViolationMonitor.add(ruleTwelveCheck, cons.setNextConstraintsWithValues());

		return panelWFDViolationMonitor;
	}

	/**
	 * 
	 */
	private JPanel getRoutingStatistics() {

		if (panelRoutingStatistics != null)
			return panelRoutingStatistics;

		int nInputColumns = 3;

		panelRoutingStatistics = new JPanel(new BorderLayout());

		TitledBorder tl = new TitledBorder("Routing Statistics");
		tl.setTitleJustification(TitledBorder.CENTER);
		tl.setTitleColor(Color.RED);
		panelRoutingStatistics.setBorder(tl);

		// panel 1
		JPanel panel1 = new JPanel(new GridBagLayout());

		GridConstraints cons = new GridConstraints(7, 3, 1);
		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(2, 2, 2, 2);
		// cons.anchor = GridBagConstraints.WEST;
		cons.anchor = GridBagConstraints.CENTER;

		// ===============================================
		JLabel lblNewLabel = new JLabel("Packets");
		// lblNewLabel.setForeground(new Color(128, 0, 0));
		panel1.add(lblNewLabel, cons.setNextConstraintsWithValues());

		// col 0
		JLabel lblSend = new JLabel("Send");
		// lblSend.setForeground(new Color(128, 0, 0));
		panel1.add(lblSend, cons.setNextConstraintsWithValues());

		JLabel lblReceive = new JLabel("Receive");
		// lblReceive.setForeground(new Color(128, 0, 0));
		panel1.add(lblReceive, cons.setNextConstraintsWithValues());

		// cons.ipadx = 8;
		// col 1
		JLabel lblR = new JLabel("AODV");
		lblR.setToolTipText("Any routing Packets");
		panel1.add(lblR, cons.setNextConstraintsWithValues());

		aodvPacketsSend = new JTextField("0", nInputColumns);
		aodvPacketsSend.setEditable(false);
		panel1.add(aodvPacketsSend, cons.setNextConstraintsWithValues());

		aodvPacketsReceive = new JTextField("0", nInputColumns);
		aodvPacketsReceive.setEditable(false);
		panel1.add(aodvPacketsReceive, cons.setNextConstraintsWithValues());

		// col 2
		JLabel lblHelloPacketsSend = new JLabel("Hello");
		lblHelloPacketsSend.setToolTipText("Hello Packets");
		panel1.add(lblHelloPacketsSend, cons.setNextConstraintsWithValues());

		helloPacketsSend = new JTextField("0", nInputColumns);
		helloPacketsSend.setEditable(false);
		panel1.add(helloPacketsSend, cons.setNextConstraintsWithValues());

		helloPacketsReceive = new JTextField("0", nInputColumns);
		helloPacketsReceive.setEditable(false);
		panel1.add(helloPacketsReceive, cons.setNextConstraintsWithValues());

		// col 3
		JLabel labelRreq = new JLabel("RREQ");
		labelRreq.setToolTipText("Route Request Packets");
		panel1.add(labelRreq, cons.setNextConstraintsWithValues());

		rreqPacketsSend = new JTextField("0", nInputColumns);
		rreqPacketsSend.setEditable(false);
		panel1.add(rreqPacketsSend, cons.setNextConstraintsWithValues());

		rreqPacketsReceive = new JTextField("0", nInputColumns);
		rreqPacketsReceive.setEditable(false);
		panel1.add(rreqPacketsReceive, cons.setNextConstraintsWithValues());

		// col 4
		JLabel rrepPackets = new JLabel("RREP");
		rrepPackets.setToolTipText("Route Reply Packets");
		panel1.add(rrepPackets, cons.setNextConstraintsWithValues());

		rrepPacketsSend = new JTextField("0", nInputColumns);
		rrepPacketsSend.setEditable(false);
		panel1.add(rrepPacketsSend, cons.setNextConstraintsWithValues());

		rrepPacketsReceive = new JTextField("0", nInputColumns);
		rrepPacketsReceive.setEditable(false);
		panel1.add(rrepPacketsReceive, cons.setNextConstraintsWithValues());

		// col 5
		JLabel lblRerrPackets = new JLabel("RERR");
		lblRerrPackets.setToolTipText("Route Error Packets");
		panel1.add(lblRerrPackets, cons.setNextConstraintsWithValues());

		rerrPacketsSend = new JTextField("0", nInputColumns);
		rerrPacketsSend.setEditable(false);
		panel1.add(rerrPacketsSend, cons.setNextConstraintsWithValues());

		rerrPacketsReceive = new JTextField("0", nInputColumns);
		rerrPacketsReceive.setEditable(false);
		panel1.add(rerrPacketsReceive, cons.setNextConstraintsWithValues());

		// col 6
		JLabel lblRavaPackets = new JLabel("RAVA");
		lblRavaPackets.setToolTipText("Route Available Packets");
		panel1.add(lblRavaPackets, cons.setNextConstraintsWithValues());

		ravaPacketsSend = new JTextField("0", nInputColumns);
		ravaPacketsSend.setEditable(false);
		panel1.add(ravaPacketsSend, cons.setNextConstraintsWithValues());

		ravaPacketsReceive = new JTextField("0", nInputColumns);
		ravaPacketsReceive.setEditable(false);
		panel1.add(ravaPacketsReceive, cons.setNextConstraintsWithValues());

		panelRoutingStatistics.add(panel1, BorderLayout.NORTH);

		// panel 2 ==================================================
		JPanel panel2 = new JPanel(new GridBagLayout());

		cons = new GridConstraints(5, 2, 1);
		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(2, 2, 2, 2);
		// cons.anchor = GridBagConstraints.WEST;
		cons.anchor = GridBagConstraints.CENTER;

		// col 0
		cons.setNextConstraintsWithValues();

		JLabel lblAll = new JLabel("All");
		panel2.add(lblAll, cons.setNextConstraintsWithValues());

		// col 1
		JLabel textfiel222 = new JLabel("RREQOrig");
		panel2.add(textfiel222, cons.setNextConstraintsWithValues());

		rreqOrigPackets = new JTextField("0", nInputColumns);
		rreqOrigPackets.setEditable(false);
		panel2.add(rreqOrigPackets, cons.setNextConstraintsWithValues());

		// col 2
		JLabel label7 = new JLabel("RREPOrig");
		panel2.add(label7, cons.setNextConstraintsWithValues());

		rrepOrigPackets = new JTextField("0", nInputColumns);
		rrepOrigPackets.setEditable(false);
		panel2.add(rrepOrigPackets, cons.setNextConstraintsWithValues());

		// col 3
		JLabel lblRsucPackets = new JLabel("RREQSucc");
		lblRsucPackets.setToolTipText("Route Request Successfull Packets");
		panel2.add(lblRsucPackets, cons.setNextConstraintsWithValues());

		rsucPackets = new JTextField("0", nInputColumns);
		rsucPackets.setEditable(false);
		panel2.add(rsucPackets, cons.setNextConstraintsWithValues());

		// col 4
		JLabel lblOtherPackets1 = new JLabel("NetMsgs");
		panel2.add(lblOtherPackets1, cons.setNextConstraintsWithValues());

		netMsgsPackets = new JTextField("0", nInputColumns);
		netMsgsPackets.setEditable(false);
		panel2.add(netMsgsPackets, cons.setNextConstraintsWithValues());

		JPanel panel2Aux = new JPanel(new BorderLayout());
		panel2Aux.add(panel2, BorderLayout.NORTH);
		panelRoutingStatistics.add(panel2, BorderLayout.CENTER);

		// ===============================================

		// rdbtnExpand = new JRadioButton("Expand");
		// rdbtnExpand.addChangeListener(new ChangeListener() {
		// public void stateChanged(ChangeEvent arg0) {
		// if (rdbtnExpand.isSelected()) {
		// frame.setSize(472, 605);
		// } else {
		// frame.setSize(472, 769);
		// }
		// }
		// });
		// rdbtnExpand.setBounds(337, 524, 109, 23);
		// frame.getContentPane().add(rdbtnExpand);

		return panelRoutingStatistics;
	}

	/**
	 * 
	 */

	public void updateStats(AodvStats currentStats) {

		// general stats
		netMsgsPackets.setText(String.valueOf(currentStats.netMsgs));
		rrepOrigPackets.setText(String.valueOf(currentStats.rrepOrig));
		rreqOrigPackets.setText(String.valueOf(currentStats.rreqOrig));
		rsucPackets.setText(String.valueOf(currentStats.rreqSucc));

		// send stats
		aodvPacketsSend.setText(String.valueOf(currentStats.send.aodvPackets));
		helloPacketsSend.setText(String.valueOf(currentStats.send.helloPackets));
		ravaPacketsSend.setText(String.valueOf(currentStats.send.ravaPackets));
		rerrPacketsSend.setText(String.valueOf(currentStats.send.rerrPackets));
		rreqPacketsSend.setText(String.valueOf(currentStats.send.rreqPackets));
		rrepPacketsSend.setText(String.valueOf(currentStats.send.rrepPackets));

		// receive stats
		aodvPacketsReceive.setText(String.valueOf(currentStats.recv.aodvPackets));
		helloPacketsReceive.setText(String.valueOf(currentStats.recv.helloPackets));
		ravaPacketsReceive.setText(String.valueOf(currentStats.recv.ravaPackets));
		rerrPacketsReceive.setText(String.valueOf(currentStats.recv.rerrPackets));
		rreqPacketsReceive.setText(String.valueOf(currentStats.recv.rreqPackets));
		rrepPacketsReceive.setText(String.valueOf(currentStats.recv.rrepPackets));

	}

	/**
	 * 
	 */
	public void incTimeOutInfo() {
		incTextField(timeOutInfo);
	}

	/**
	 * 
	 */
	public void incWFDRuleCounter(int ruleNumber) {
		JTextField[] ruleFields = { ruleOneCheck, ruleTwoCheck, ruleThreeCheck, ruleFourCheck, ruleFiveCheck,
				ruleSixCheck, ruleSevenCheck, ruleEightCheck, ruleNineCheck, ruleTenCheck, ruleElevenCheck,
				ruleTwelveCheck };

		incTextField(ruleFields[ruleNumber - 1]);
	}

	/**
	 * 
	 */
	private void incTextField(JTextField tf) {
		tf.setText(String.valueOf(Integer.parseInt(tf.getText()) + 1));
	}

	public void setShortestPathCount(long pathCounter) {
		shortestPathCount.setText(String.valueOf(pathCounter));
	}

	public void setAvShortestPath(double pathLength) {
		avShortestPath.setText(String.valueOf(pathLength));
	}

	public void setSimTime(String simTimeValue) {
		simTime.setText(simTimeValue);
	}

	public void setMaxNodeSpeed(double maxspeed) {
		maxNodeSpeed.setText(String.valueOf(maxspeed));
	}

	public void setMinNodeSpeed(double minspeed) {
		minNodeSpeed.setText(String.valueOf(minspeed));
	}

	public void setNetSize(int size) {
		netSize.setText(String.valueOf(size));
	}

	public void setRealTime(String time) {
		realTime.setText(time);
	}

	public void setNumGroups(long noGroups) {
		numGroups.setText(String.valueOf(noGroups));
	}

	public void setNumConnectedNodes(double connectedNodes) {
		numConnectedNodes.setText(String.valueOf(connectedNodes));
	}

	public void setGOMaxNumClients(int maxClient) {
		numclients.setText(String.valueOf(maxClient));

	}

	public void setNodeMaxNeighbors(int maxNeighbor2) {
		maxNeighbor.setText(String.valueOf(maxNeighbor2));

	}

	public void setLastCycleTimeMs(long time) {
		lastCycleTimeMs.setText(String.valueOf(time));

	}

	public void setCurrentCycle(long currentCycleNumber) {
		currentCycle.setText(String.valueOf(currentCycleNumber));
	}

	public void setConnectivity(double connectivityPresent) {
		connectivityTextField.setText(String.valueOf(connectivityPresent));
	}

	public void setAvgConnectivity(double d) {
		avgConnectivityTextField.setText(String.valueOf(d));

	}

	void setEndOfSimulation() {
		currentCycle.setBackground(Color.red);
		currentCycle.setOpaque(true);
		panelSimulatorControl.setVisible(false);
	}
}

/**
 * 
 */
class GridConstraints extends GridBagConstraints {
	private static final long serialVersionUID = 1L;

	int nMainCols;
	int nMainLines;
	int nCols;

	/**
	 * 
	 */
	public GridConstraints(int nMainCols, int nMainLines, int nCols) {
		this.nMainCols = nMainCols;
		this.nMainLines = nMainLines;
		this.nCols = nCols;

		// init values
		gridx = gridy = -1;
	}

	/**
	 * 
	 */
	GridConstraints setNextConstraintsWithValues() {
		if (gridx == -1) {
			gridx = gridy = 0;
		} else

		if (++gridx % nCols == 0) {
			gridx -= nCols;
			if (++gridy % nMainLines == 0) {
				gridy = 0;
				gridx += nCols;
			}
		}

		// System.out.println("x-> " + gridx + ", y-> " + gridy);
		return this;
	}

}
