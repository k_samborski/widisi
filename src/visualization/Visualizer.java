/*
 * Copyright (c) 2014-2015 SCUBE Joint Open Lab
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * Author: Naser Derakhshan
 * Politecnico di Milano
 * Condition: Do not remove this head
 */
package visualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.preview.PNGExporter;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.plugins.layout.geo.GeoLayout;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantColor;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.ranking.api.Ranking;
import org.gephi.ranking.api.RankingController;
import org.gephi.ranking.api.Transformer;
import org.gephi.ranking.plugin.transformer.AbstractColorTransformer;
import org.gephi.ranking.plugin.transformer.AbstractSizeTransformer;
import org.gephi.ranking.plugin.transformer.AbstractTransformer;
import org.gephi.statistics.plugin.GraphDistance;
import org.openide.util.Lookup;

import applications.magnet.Routing.AodvStats;
import it.uniroma1.dis.wsngroup.gexf4j.core.EdgeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.Attribute;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeClass;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeList;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.data.AttributeListImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.viz.ColorImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.viz.PositionImpl;
import nodemovement.CoordinateKeeper;
import nodemovement.NodeMovement;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import wifi.WifiManager;
import wifidirect.p2pcore.nodeP2pInfo;

/**
 * The Class Visualizer.
 */
public class Visualizer implements Control {

	// ================================================================
	// Class constants ================================================

	/** The Constant CONNECTED. */
	public static final int CONNECTED = 0;

	/** The Constant INVITED. */
	public static final int INVITED = 1;

	/** The Constant FAILED. */
	public static final int FAILED = 2;

	/** The Constant AVAILABLE. */
	public static final int AVAILABLE = 3;

	/** The Constant UNAVAILABLE. */
	public static final int UNAVAILABLE = 4;

	/** Nodes colors */
	public static final ColorImpl NodeGOConnectedColor = new ColorImpl(0, 255, 0);
	public static final ColorImpl NodeGOConnectedWFColor = new ColorImpl(0, 0, 0);
	public static final ColorImpl NodeCRColor = new ColorImpl(255, 0, 0);
	public static final ColorImpl NodeCLColor = new ColorImpl(0, 0, 254);
	public static final ColorImpl NodeLCColor = new ColorImpl(60, 180, 180);
	public static final ColorImpl NodeNoGOConnectedColor = new ColorImpl(0, 0, 255);
	public static final ColorImpl NodePeerDiscoveryStateColor = new ColorImpl(150, 150, 150);
	public static final ColorImpl NodeUnavailableColor = new ColorImpl(255, 255, 255);
	public static final ColorImpl NodeInvitedPeerDiscColor = new ColorImpl(0, 255, 255);
	public static final ColorImpl NodeAvailableDiscColor = new ColorImpl(150, 150, 150);
	public static final ColorImpl NodeDefaultColor = new ColorImpl(100, 100, 100);
	public static final ColorImpl NodeReferenceNodeColor = new ColorImpl(255, 0, 0);

	/** Links colors */
	public static final ColorImpl linkWFDColor = new ColorImpl(0, 0, 255);
	public static final ColorImpl linkWFColor = new ColorImpl(255, 0, 0);
	public static final ColorImpl LinkFreeColor = new ColorImpl(220, 220, 220);

	private static final long SHOW_NETWORK_DELTA_TIME_MS = 1000;

	private static final float NODES_MIN_SIZE_FACTOR = 0.1f;

	private static final float NODES_MAX_SIZE_FACTOR = 0.25f;

	/** Scaling factor for all nodes */
	/** GephiNodeSize = FieldLength / NODE_SIZE_FACTOR; */
	private static final int NODE_SIZE_FACTOR = 10;

	/** Go node size, its relative to base node size */
	/** goSize = GephiNodeSize * GO_NODES_SIZE_FACTOR */
	private static final float GO_NODES_SIZE_FACTOR = 1.6f;

	// ================================================================
	// Class attributes ===============================================

	private static Visualizer visualizer = null;

	public static List<it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiNodeGroup;

	// used externally
	public static ColorImpl[] pNodeColor = new ColorImpl[1000];

	/** The Cycle length. */
	public static int CycleLength = 100;

	private boolean scrollOutput = true;

	public VisualizerControls visualizerControls;

	public double connectivityAvg = 0;

	public long connValNum = 0;

	/** The output. This should much better if not static */
	private JTextArea output = null;

	// ================================================================
	// Instance attributes ============================================

	/** The coordinates pid. */
	private int coordinatesPid;

	/** The linkable id. */
	private int linkableId;

	private int wifimanagerPid;

	/** The p2p info pid. */
	private int p2pInfoPid;

	/** The Field length. */
	private int FieldLength;

	/** The Gephi size. */
	private int GephiNodeSize;

	/** The maxspeed. */
	private double maxspeed;

	/** The minspeed. */
	private double minspeed;

	/** The start time real. */
	private long startTimeReal = 0;

	/** The last cycle start time real. */
	private long startCycleTimeReal = 0;

	/** The cycle. */
	private long cycle = 0;

	/** The frame. */
	private JFrame frame = null;

	/** The image. */
	private Image image = null;

	/** The label. */
	private JLabel labelNetwork = null;

	/** flag used to save network image file */
	protected boolean isToSaveNetworkImageToFile = false;

	/** Zoom factor */
	private double zoomFactor = 1.0;

	private double zoomDeltaFactor = 0.1;

	/** Initial LABELNETWORK preferred size for zoom operations */
	Dimension prefSize;

	/** Show Linkable Connection on network window */
	protected boolean showLinkableConnections = true;

	private JCheckBoxMenuItem showControlWFDRulesPanelMenuItem;

	private JCheckBoxMenuItem showControlRoutingSatisticsPanelMenuItem;

	private JCheckBoxMenuItem showNetworkMenuItem;

	protected boolean showNetwork = true;

	private JCheckBoxMenuItem debugConsoleScrollMenuItem;

	private boolean enableDebugDataToConsole = true;

	protected boolean stopSimulator = false;

	private long lastShowNetworkTime;

	private JCheckBoxMenuItem showControlNetworkControlPanelMenuItem;

	private JCheckBoxMenuItem stopSimulatorMenuItem;

	private JCheckBoxMenuItem enableDebugDataToConsoleMenuItem;

	private long endtime;

	private int RadioRangeLength;

	private boolean showLabels = true;

	private JCheckBoxMenuItem pauseSimulatorMenuItem;

	private Semaphore semPauseSimulator = new Semaphore(1, true);

	private boolean isSimulatorPaused = false;

	private boolean isStepUntilActive = false;

	private long stepRunUntilCycle = -1;

	private boolean isRunUntilActive = false;

	// ================================================================
	// METHODS ========================================================

	/**
	 * Instantiates a new visualizer.
	 *
	 * @param prefix
	 *            the prefix
	 */
	public Visualizer(String prefix) {
		visualizer = this;

		p2pInfoPid = Configuration.getPid(prefix + "." + "p2pinfo");
		linkableId = Configuration.getPid(prefix + "." + "linkable");
		wifimanagerPid = Configuration.getPid(prefix + "." + "wifimanager");
		coordinatesPid = Configuration.getPid(prefix + "." + "coord");
		endtime = Configuration.getLong("simulation.endtime");
		FieldLength = Configuration.getInt("FIELDLENGTH");
		RadioRangeLength = Configuration.getInt("RADIORANGELENGTH");

		System.out.println("Visualizer constructor =============================");
		System.out.println("Visualizer -> " + prefix);

		buildVisualizerMainFrame();
	}

	/**
	 * 
	 */
	public boolean execute() {

		visualizerControls.setCurrentCycle(CommonState.getTime());

		doStepRunUntilSimulator();

		// System.out.println("isEventDispatchThread -> " +
		// SwingUtilities.isEventDispatchThread());

		// System.out.println("Cycle: " + cycle + " time: " +
		// CommonState.getTime() + " phase: " + CommonState.getPhase()
		// + " int time: " + CommonState.getIntTime());

		CycleLength = (int) NodeMovement.CycleLenght;

		GephiNodeSize = FieldLength / NODE_SIZE_FACTOR;
		maxspeed = NodeMovement.SpeedMx; // meter/sec
		minspeed = NodeMovement.SpeedMn; // meter/sec

		if (cycle == 0) {
			initVisualizer();
		}

		long systemTime = System.currentTimeMillis();
		if ((isSimulatorPaused && !isRunUntilActive)
				|| systemTime - lastShowNetworkTime > SHOW_NETWORK_DELTA_TIME_MS && showNetwork) {
			lastShowNetworkTime = systemTime;
			showNetwork();
		}

		if (cycle > 1) {
			doVisualizerStatsWorkCycle();
		}

		cycle++;

		if (cycle == endtime) {
			// end of simulation
			visualizerControls.setEndOfSimulation();
		}

		if (stopSimulator)
			return true;

		return false;
	}

	/**
	 * This is done for cycle Simulator (CDSim). For event driven simulator,
	 * this should be analyzed.
	 */
	private void doStepRunUntilSimulator() {
		if ((isStepUntilActive || isRunUntilActive) && cycle == stepRunUntilCycle) {
			isStepUntilActive = isRunUntilActive = false;
			visualizerControls.setEnableStepRunUntilButtons(true);
			pauseSimulator();
		}

		// enable pause simulator
		try {
			semPauseSimulator.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		semPauseSimulator.release();
	}

	/**
	 * Do visualizer initialization. Called once at cycle 1
	 * 
	 * @param fieldlength
	 */
	private void initVisualizer() {
		ColorImpl BlackColor = new ColorImpl(0, 0, 0);

		// ???
		for (int i = 0; i < pNodeColor.length; i++) {
			pNodeColor[i] = BlackColor;
		}

		startTimeReal = startCycleTimeReal = System.currentTimeMillis();
	}

	/**
	 * do one visualizer work cycle
	 */
	private void doVisualizerStatsWorkCycle() {
		visualizerControls.setMaxNodeSpeed(maxspeed);
		visualizerControls.setMinNodeSpeed(minspeed);

		long thisCycleStartTime = System.currentTimeMillis();

		double connectedDevices = 0;
		long noGroups = 0;
		int maxClient = 0;
		int maxNeighbor = 0;

		for (int i = 0; i < Network.size(); i++) {
			Node node = Network.get(i);
			nodeP2pInfo nodeInfo2 = (nodeP2pInfo) node.getProtocol(p2pInfoPid);
			Linkable neighbor = (Linkable) node.getProtocol(linkableId);

			if (neighbor.degree() > maxNeighbor) {
				maxNeighbor = neighbor.degree();
			}
			if (nodeInfo2.getStatus() == CONNECTED) {
				connectedDevices++;
			}
			if (nodeInfo2.isGroupOwner()) {
				noGroups++;
				if (nodeInfo2.currentGroup.getGroupSize() > maxClient) {
					maxClient = nodeInfo2.currentGroup.getGroupSize();
				}
			}
		}

		// calculate the real time
		long elapsedTime = thisCycleStartTime - startTimeReal;
		long elapsedSeconds = elapsedTime / 1000;
		int hour = (int) (elapsedSeconds / 3600);
		long remain = elapsedSeconds % 3600;
		int min = (int) remain / 60;
		int sec = (int) remain % 60;
		visualizerControls.setRealTime(hour + ":" + min + ":" + sec);

		// calculate the simulator time
		elapsedTime = (CycleLength * CommonState.getTime());
		elapsedSeconds = elapsedTime / 1000;
		hour = (int) (elapsedSeconds / 3600);
		remain = elapsedSeconds % 3600;
		min = (int) remain / 60;
		sec = (int) remain % 60;
		visualizerControls.setSimTime(hour + ":" + min + ":" + sec);

		// cycle time
		long cycleTimeReal = thisCycleStartTime - startCycleTimeReal;
		startCycleTimeReal = thisCycleStartTime;
		visualizerControls.setLastCycleTimeMs(cycleTimeReal);

		visualizerControls.setNetSize(Network.size());
		visualizerControls.setNumGroups(noGroups);
		visualizerControls.setNumConnectedNodes((connectedDevices / Network.size()) * 100);
		visualizerControls.setGOMaxNumClients(maxClient);
		visualizerControls.setNodeMaxNeighbors(maxNeighbor);
	}

	/**
	 * Show image.
	 *
	 */
	private void showImage(ByteArrayOutputStream baos) throws IOException {
		byte[] png = baos.toByteArray();

		image = ImageIO.read(new ByteArrayInputStream(png));
		// System.out.println("SHOW IMAGE.....");

		// save image on file if requested
		if (isToSaveNetworkImageToFile) {
			isToSaveNetworkImageToFile = false;
			// Write image to file
			System.out.println("Writing network image to file...........");
			File outDir = new File("./screenshots");
			if (!outDir.exists())
				outDir.mkdir();
			File f = new File("./screenshots/" + cycle + ".png");

			ImageIO.write((BufferedImage) image, "png", f);
		}

		ImageIcon img = new ImageIcon(image);

		labelNetwork.setIcon(img);

		// keep initial preferred size for zoom operations
		prefSize = labelNetwork.getPreferredSize();

		updateImage();
	}

	/**
	 * 
	 */
	private void buildVisualizerMainFrame() {
		frame = new JFrame();
		frame.setTitle("WiDiSi Visualizer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.setAlwaysOnTop(true);

		// controls area
		visualizerControls = new VisualizerControls(this, FieldLength, RadioRangeLength);
		frame.getContentPane().add(visualizerControls, BorderLayout.WEST);

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Debug output area
		JPanel panelOutput = new JPanel(new BorderLayout());
		output = new JTextArea(20, 30);
		output.setEditable(false);
		output.setLineWrap(true);
		output.append("");
		panelOutput.setBorder(new TitledBorder("Debug Pane"));
		JScrollPane outputScrollPane = new JScrollPane(output);
		panelOutput.add(outputScrollPane, BorderLayout.CENTER);

		// Network Visualizer area
		JPanel panelNetVisualizer = new JPanel(new BorderLayout());
		panelNetVisualizer.setBorder(new TitledBorder("Network Pane"));
		labelNetwork = new JLabel();
		JScrollPane netVisualizerScrollPane = new JScrollPane(labelNetwork);
		panelNetVisualizer.add(netVisualizerScrollPane, BorderLayout.CENTER);
		netVisualizerScrollPane.setWheelScrollingEnabled(false);

		labelNetwork.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				double wheelRotValue = e.getPreciseWheelRotation();
				System.out.println(e.getWheelRotation() + " " + wheelRotValue);
				adjustZoomFactor(wheelRotValue);
			}
		});

		splitPane.setLeftComponent(panelOutput);
		splitPane.setRightComponent(panelNetVisualizer);
		frame.getContentPane().add(splitPane, BorderLayout.CENTER);

		// to get good dimensions and center frame
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize((int) (screenSize.getWidth() / 1.2), (int) (screenSize.getHeight() / 1.2));
		frame.setLocationRelativeTo(null);

		// being resized, set divider location
		splitPane.setDividerLocation((int) output.getPreferredSize().getWidth());

		// place menu bar
		frame.setJMenuBar(getVisualizerMenuBar());

		// set visible
		frame.setVisible(true);
	}

	/**
	 * Adjust Zoom with new mouse wheel rotation value
	 * 
	 */
	private void adjustZoomFactor(double wheelRotValue) {
		if (wheelRotValue > 0.2)
			zoomFactor += zoomDeltaFactor;
		if (wheelRotValue < -0.2)
			zoomFactor -= zoomDeltaFactor;

		updateImage();

		// labelNetwork.revalidate();
		System.out.println("Zoom: " + zoomFactor);
	}

	/**
	 * Update image with zoom factor
	 * 
	 * AT: I gave up from this, as scaling the image takes a lot of CPU. It is
	 * needed a better solution. Probably not using images: draw nodes.
	 */
	private void updateImage() {
		// labelNetwork.setIcon(new ImageIcon(image.getScaledInstance((int)
		// (prefSize.width * zoomFactor),
		// (int) (prefSize.height * zoomFactor), Image.SCALE_SMOOTH)));
	}

	/**
	 * 
	 */
	private JMenuBar getVisualizerMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem saveAsMenuItem = new JMenuItem("Save As");
		fileMenu.add(saveAsMenuItem);

		JMenuItem getNetworkImageFileMenuItem = new JMenuItem("Get Network Image File");
		fileMenu.add(getNetworkImageFileMenuItem);

		fileMenu.addSeparator();

		showNetworkMenuItem = new JCheckBoxMenuItem("Show (build) network", showNetwork = true);
		fileMenu.add(showNetworkMenuItem);

		final JCheckBoxMenuItem showLinkableConnectionsMenuItem = new JCheckBoxMenuItem("Show Linkable Connections");
		fileMenu.add(showLinkableConnectionsMenuItem);
		showLinkableConnectionsMenuItem.setSelected(showLinkableConnections = true);

		final JCheckBoxMenuItem showLabelsMenuItem = new JCheckBoxMenuItem("Show Node Label", showLabels = false);
		fileMenu.add(showLabelsMenuItem);

		fileMenu.addSeparator();

		showControlNetworkControlPanelMenuItem = new JCheckBoxMenuItem("Show Control Network Manager Panel", false);
		fileMenu.add(showControlNetworkControlPanelMenuItem);
		visualizerControls.showPanelNetworkControl(showControlNetworkControlPanelMenuItem.isSelected());

		showControlWFDRulesPanelMenuItem = new JCheckBoxMenuItem("Show Control WFD rules", true);
		fileMenu.add(showControlWFDRulesPanelMenuItem);

		showControlRoutingSatisticsPanelMenuItem = new JCheckBoxMenuItem("Show Control Routing stats", true);
		fileMenu.add(showControlRoutingSatisticsPanelMenuItem);

		fileMenu.addSeparator();

		pauseSimulatorMenuItem = new JCheckBoxMenuItem("Pause Simulator");
		fileMenu.add(pauseSimulatorMenuItem);

		stopSimulatorMenuItem = new JCheckBoxMenuItem("Stop Simulator");
		fileMenu.add(stopSimulatorMenuItem);

		enableDebugDataToConsoleMenuItem = new JCheckBoxMenuItem("Enable debug data to console", true);
		fileMenu.add(enableDebugDataToConsoleMenuItem);

		debugConsoleScrollMenuItem = new JCheckBoxMenuItem("Debug Console: scroll with append", true);
		fileMenu.add(debugConsoleScrollMenuItem);

		JMenuItem exitMenuItem = new JMenuItem("Exit");
		fileMenu.add(exitMenuItem);

		// Menu icons

		// ---------------------------

		// the global action listener
		ActionListener alMenu = new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				JMenuItem mi = (JMenuItem) ae.getSource();
				switch (mi.getText()) {

				case "Pause Simulator":
					pauseSimulatorMenuItem.setText("Resume Simulator");
					pauseSimulator();
					break;

				case "Resume Simulator":
					pauseSimulatorMenuItem.setText("Pause Simulator");
					resumeSimulator();
					break;

				case "Stop Simulator":
					stopSimulator();
					break;

				case "Show (build) network":
					showNetwork = showNetworkMenuItem.isSelected();
					break;

				case "Show Linkable Connections":
					showLinkableConnections = showLinkableConnectionsMenuItem.isSelected();
					if (isSimulatorStopped())
						showNetwork();
					break;

				case "Show Node Label":
					showLabels = !showLabels;
					if (isSimulatorStopped())
						showNetwork();
					break;

				case "Show Control Network Manager Panel":
					visualizerControls.showPanelNetworkControl(showControlNetworkControlPanelMenuItem.isSelected());
					break;

				case "Show Control WFD rules":
					visualizerControls.showPanelWFDRules(showControlWFDRulesPanelMenuItem.isSelected());
					break;

				case "Show Control Routing stats":
					visualizerControls
							.showPanelRoutingStatistics(showControlRoutingSatisticsPanelMenuItem.isSelected());
					break;

				case "Get Network Image File":
					isToSaveNetworkImageToFile = true;
					break;

				case "Enable debug data to console":
					// enableDebugDataToConsoleMenuItem.isSelected();
					enableDebugDataToConsole = !enableDebugDataToConsole;
					System.out.println("enableDebugDataToConsole -> " + enableDebugDataToConsole);
					break;

				case "Debug Console: scroll with append":
					scrollOutput = debugConsoleScrollMenuItem.isSelected();
					break;

				case "Exit":
					// send close to window
					frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
					break;
				}
			}
		};

		// add listener to menu items
		exitMenuItem.addActionListener(alMenu);
		stopSimulatorMenuItem.addActionListener(alMenu);
		enableDebugDataToConsoleMenuItem.addActionListener(alMenu);
		debugConsoleScrollMenuItem.addActionListener(alMenu);
		getNetworkImageFileMenuItem.addActionListener(alMenu);
		showLinkableConnectionsMenuItem.addActionListener(alMenu);
		showControlWFDRulesPanelMenuItem.addActionListener(alMenu);
		showControlRoutingSatisticsPanelMenuItem.addActionListener(alMenu);
		showControlNetworkControlPanelMenuItem.addActionListener(alMenu);
		showLabelsMenuItem.addActionListener(alMenu);
		showNetworkMenuItem.addActionListener(alMenu);
		pauseSimulatorMenuItem.addActionListener(alMenu);

		return menuBar;
	}

	/**
	 * 
	 */
	protected void pauseSimulator() {
		try {
			semPauseSimulator.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		isSimulatorPaused = true;
	}

	/**
	 * 
	 */
	protected void resumeSimulator() {
		semPauseSimulator.release();
		isSimulatorPaused = false;
	}

	/**
	 * 
	 */
	protected void activateStepUntilSimulator() {
		semPauseSimulator.release();
	}

	/**
	 * 
	 */
	public void stopSimulator() {
		stopSimulatorMenuItem.setEnabled(false);
		stopSimulator = true;
		visualizerControls.setEndOfSimulation();
	}

	/**
	 * 
	 */
	public boolean isSimulatorStopped() {
		return stopSimulator;
	}

	/**
	 * Put nodes on gephi layer
	 */
	private void putNodesOnGephiMap(List<it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiNode,
			HashMap<Long, it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiMap, Graph graph, Attribute longitude,
			Attribute latitude) {
		Node node;

		// put nodes
		for (int i = 0; i < Network.size(); i++) {
			node = (Node) Network.get(i);
			nodeP2pInfo nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);
			WifiManager wifiManager = (WifiManager) node.getProtocol(wifimanagerPid);
			CoordinateKeeper coordinate = (CoordinateKeeper) node.getProtocol(coordinatesPid);

			// define node color
			ColorImpl nodeColor = NodeDefaultColor;

			if (nodeInfo.isGroupOwner() && nodeInfo.getStatus() == CONNECTED
					&& wifiManager.getWifiStatus() != CONNECTED) {
				nodeColor = NodeGOConnectedColor;

			} else if (nodeInfo.isGroupOwner() && nodeInfo.getStatus() == CONNECTED
					&& wifiManager.getWifiStatus() == CONNECTED) {
				nodeColor = NodeGOConnectedWFColor;

			} else if (!nodeInfo.isGroupOwner() && nodeInfo.getStatus() == CONNECTED
					&& wifiManager.getWifiStatus() == CONNECTED) {
				nodeColor = NodeCRColor;

			} else if (!nodeInfo.isGroupOwner() && nodeInfo.getStatus() == CONNECTED
					&& wifiManager.getWifiStatus() != CONNECTED) {
				nodeColor = NodeCLColor;

			} else if (!nodeInfo.isGroupOwner() && nodeInfo.getStatus() != CONNECTED
					&& wifiManager.getWifiStatus() == CONNECTED) {
				nodeColor = NodeLCColor;

			} else if (nodeInfo.getStatus() == INVITED && nodeInfo.isPeerDiscoveryStarted()) {
				nodeColor = NodeInvitedPeerDiscColor;

			} else if (nodeInfo.getStatus() == AVAILABLE && nodeInfo.isPeerDiscoveryStarted()) {
				nodeColor = NodeAvailableDiscColor;

			} else if ((nodeInfo.getStatus() == UNAVAILABLE)) {
				nodeColor = NodeUnavailableColor;
			}

			// Enforce coordinates not larger than 1 or -1 (double check)
			float signx = coordinate.getX() < 0 ? -1f : 1f;
			float signy = coordinate.getY() < 0 ? -1f : 1f;
			float nodeX = (Math.abs(coordinate.getX()) > 1.0 ? signx : (float) coordinate.getX()) * FieldLength;
			float nodeY = (Math.abs(coordinate.getY()) > 1.0 ? signy : (float) coordinate.getY()) * FieldLength;
			PositionImpl nodePosition = new PositionImpl(nodeX, nodeY, 0);

			// create gephi node on graph, put it on list gephiNode and on
			// gephiMap
			gephiNode.add(i, graph.createNode(String.valueOf(node.getID())));
			gephiNode.get(i).setLabel(String.valueOf(node.getID())).setSize(GephiNodeSize).setPosition(nodePosition)
					.setColor(nodeColor).getAttributeValues().addValue(longitude, String.valueOf(nodePosition.getX()))
					.addValue(latitude, String.valueOf(nodePosition.getY()));

			if (nodeInfo.isGroupOwner())
				gephiNode.get(i).setSize(GephiNodeSize * GO_NODES_SIZE_FACTOR);

			// it.uniroma1.dis.wsngroup.gexf4j.core.Node aux =
			// graph.createNode();
			// aux.setColor(NodeLCColor).setSize(GephiSize).getAttributeValues().addValue(longitude,
			// String.valueOf(nodePosition.getX()))
			// .addValue(latitude, String.valueOf(nodePosition.getY()));

			gephiMap.put(node.getID(), gephiNode.get(i));
		}
	}

	/**
	 * 
	 */
	private void putNetworkLinkableLinksOnGephiMap(HashMap<Long, it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiMap) {

		// run over all network nodes
		for (int i = 0; i < Network.size(); i++) {
			Node node = Network.get(i);
			Linkable possibleLinks = (Linkable) node.getProtocol(linkableId);

			// check every possible link, and if not connected, connected it
			// with LinkFreeColor
			for (int j = 0; j < possibleLinks.degree(); j++) {
				if (!gephiNodeGroup.get(i).hasEdgeTo(gephiMap.get(possibleLinks.getNeighbor(j).getID()).getId())) {
					gephiNodeGroup.get(i).connectTo(gephiMap.get(possibleLinks.getNeighbor(j).getID()))
							.setEdgeType(EdgeType.UNDIRECTED).setColor(LinkFreeColor);
				}
			}
		}
	}

	/**
	 * Puts 4 dump gephi nodes at the 4 end of cartesian system in order to see
	 * the movement
	 */
	private void putReferenteNodesOnGephiGraph(List<it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiNode, Graph graph,
			Attribute longitude, Attribute latitude) {

		PositionImpl[] refNodes = new PositionImpl[] { new PositionImpl(0, FieldLength, 0),
				new PositionImpl(FieldLength, FieldLength, 0), new PositionImpl(FieldLength, 0, 0),
				new PositionImpl(0, 0, 0), new PositionImpl(RadioRangeLength, FieldLength, 0) };
		ColorImpl nodeColor = NodeReferenceNodeColor;
		String[] nodeNames = { "P0", "P1", "P2", "P3", "RC" };

		for (int i = 0; i < refNodes.length; i++) {
			PositionImpl nodePosition = refNodes[i];

			gephiNode.add(Network.size() + i, graph.createNode());
			gephiNode.get(Network.size() + i).setLabel(nodeNames[i]).setSize(GephiNodeSize).setPosition(nodePosition)
					.setColor(nodeColor).getAttributeValues().addValue(longitude, String.valueOf(nodePosition.getX()))
					.addValue(latitude, String.valueOf(nodePosition.getY()));
		}

	}

	/**
	 * Show network
	 */
	private void showNetwork() {
		// generating .gexf file using gexf library
		Gexf gexf = new GexfImpl();

		// getting current date for gexf - optional
		Calendar date = Calendar.getInstance();

		// Set gexf necessary fields
		gexf.getMetadata().setLastModified(date.getTime()).setCreator("NaserDerakhshan.org")
				.setDescription("Wifi Direct Simulator");
		gexf.setVisualization(true);

		// This Graph class is taken from gexf library
		Graph graph = gexf.getGraph();

		// attribute is needed so that the gephi importer find the necessary
		// fields for longitude-latitude
		AttributeList attrList = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(attrList);
		Attribute longitude = attrList.createAttribute("longitude", AttributeType.FLOAT, "longitude");
		Attribute latitude = attrList.createAttribute("latitude", AttributeType.FLOAT, "latitude");

		// A list of Gephi nodes - We used long identifier to distinguish gephi
		// Node from peerSim Node
		gephiNodeGroup = new ArrayList<it.uniroma1.dis.wsngroup.gexf4j.core.Node>();
		// A HashMap that map peersim nodeID to the gephi node
		HashMap<Long, it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiMap = new HashMap<Long, it.uniroma1.dis.wsngroup.gexf4j.core.Node>();

		//////////////////////////////////////
		// PUT data on Gephi map

		// put network node on gephi map
		putNodesOnGephiMap(gephiNodeGroup, gephiMap, graph, longitude, latitude);

		// put node colors on this array - used outside. TODO do it more OOP
		for (int i = 0; i < Network.size(); i++) {
			pNodeColor[i] = (ColorImpl) gephiNodeGroup.get(i).getColor();
		}

		// put reference nodes
		putReferenteNodesOnGephiGraph(gephiNodeGroup, graph, longitude, latitude);

		// put connected links on Gephi Map
		putNetworkConnectedLinksOnGephiMap(gephiMap);

		if (showLinkableConnections)
			putNetworkLinkableLinksOnGephiMap(gephiMap);

		// printNetworkOnDebugPane();

		//////////////////////////////////////

		// generate output gexf file from the above mentioned graph
		StaxGraphWriter graphWriter = new StaxGraphWriter();
		try {
			File tempFile = File.createTempFile("tempStream", ".gexf");
			tempFile.deleteOnExit();
			Writer out = new FileWriter(tempFile, false);
			graphWriter.writeToStream(gexf, out, "UTF-8");

			// get import controller
			ImportController importController = Lookup.getDefault().lookup(ImportController.class);

			// Container and its properties
			Container container = importController.importFile(tempFile);
			container.getLoader().setEdgeDefault(EdgeDefault.MIXED);
			container.setAllowAutoNode(false); // Don�t create missing nodes
			container.setAutoScale(false);

			// Import the created graph.gexf file to the gephi library
			// Init a project - and therefore a workspace
			ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
			pc.newProject();
			Workspace workspace = pc.getCurrentWorkspace();

			// Append imported data to GraphAPI
			importController.process(container, new DefaultProcessor(), workspace);

			// Get graph model of current workspace
			GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();

			// TODO
			// update shortest path available
			// AttributeModel attributeModel =
			// Lookup.getDefault().lookup(AttributeController.class).getModel();
			// updateShortestPathAvailable(graphModel, attributeModel);

			// Layout based on GeoLayout Plugin for Gephi
			GeoLayout layout = new GeoLayout(null);
			layout.setGraphModel(graphModel);
			layout.resetPropertiesValues();
			layout.setCentered(true);
			// layout.setScale((double) FieldLength);
			layout.setProjection("Equirectangular");
			layout.initAlgo();

			layout.goAlgo();

			// Preview
			PreviewModel model = Lookup.getDefault().lookup(PreviewController.class).getModel();
			PreviewProperties prevProp = model.getProperties();

			if (showLabels)
				prevProp.putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
			prevProp.putValue(PreviewProperty.NODE_LABEL_PROPORTIONAL_SIZE, Boolean.TRUE);

			prevProp.putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(EdgeColor.Mode.ORIGINAL));
			prevProp.putValue(PreviewProperty.NODE_BORDER_COLOR, new DependantColor(Color.BLACK));

			prevProp.putValue(PreviewProperty.EDGE_THICKNESS, new Float(FieldLength * 0.02));
			prevProp.putValue(PreviewProperty.ARROW_SIZE, new Float(10f));

			prevProp.putValue(PreviewProperty.EDGE_OPACITY, new Float(100));
			prevProp.putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
			// prevProp.putValue(PreviewProperty.SHOW_EDGE_LABELS,
			// Boolean.TRUE);
			prevProp.putValue(PreviewProperty.EDGE_LABEL_OUTLINE_SIZE, new Float(18));
			prevProp.putValue(PreviewProperty.EDGE_LABEL_OUTLINE_OPACITY, new Float(50));
			prevProp.putValue(PreviewProperty.EDGE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));
			// prevProp.putValue(PreviewProperty.EDGE_LABEL_FONT,
			// prevProp.getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(3));
			// model.getProperties().putValue(PreviewProperty.NODE_BORDER_WIDTH,
			// new Float(18));
			prevProp.putValue(PreviewProperty.NODE_LABEL_OUTLINE_SIZE, new Float(18));
			prevProp.putValue(PreviewProperty.NODE_LABEL_OUTLINE_OPACITY, new Float(50));
			prevProp.putValue(PreviewProperty.NODE_LABEL_FONT,
					prevProp.getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));

			// Rank nodes
			// RankingController rankingController =
			// Lookup.getDefault().lookup(RankingController.class);
			// Ranking<?> degreeRanking =
			// rankingController.getModel().getRanking(Ranking.NODE_ELEMENT,
			// Ranking.DEGREE_RANKING);
			// Rank by color
			// transformer = getColorTransformer(rankingController);
			// Rank in size, by number of IN Edges (Indegree)
			// AbstractTransformer<?> transformer =
			// getSizeTransformer(rankingController);
			// rankingController.transform(degreeRanking, transformer);

			// Export and show on JFrame
			ExportController ec = Lookup.getDefault().lookup(ExportController.class);
			// ec.exportFile(new File("log/autolayout.png"));
			PNGExporter exporter = (PNGExporter) ec.getExporter("png");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ec.exportStream(baos, exporter);
			showImage(baos);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void printNetworkOnDebugPane() {
		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			nodeP2pInfo nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);
			if (nodeInfo.getStatus() == CONNECTED && !nodeInfo.isGroupOwner()
					&& gephiNodeGroup.get(i).getEdges().isEmpty()) {
				print("Node: " + node.getID() + " GO: " + nodeInfo.getGroupOwner().getID());
				nodeP2pInfo neighborInfo = (nodeP2pInfo) nodeInfo.getGroupOwner().getProtocol(p2pInfoPid);
				print(neighborInfo.currentGroup.getNodeList().contains(node));
				print("Nodes inside this Group: ");
				for (Node tempNode : neighborInfo.currentGroup.getNodeList()) {
					print(tempNode.getID());

				}
			}
		}
	}

	/**
	 * Transformer to rank in size, by number of IN edges (?)
	 */
	private AbstractTransformer<?> getSizeTransformer(RankingController rankingController) {

		AbstractSizeTransformer<?> sizeTransformer = (AbstractSizeTransformer<?>) rankingController.getModel()
				.getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
		sizeTransformer.setMinSize(NODES_MIN_SIZE_FACTOR * FieldLength);
		sizeTransformer.setMaxSize(NODES_MAX_SIZE_FACTOR * FieldLength);
		return sizeTransformer;
	}

	/**
	 * Transformer to rank in color, by ?
	 */
	@SuppressWarnings("unused")
	private AbstractTransformer<?> getColorTransformer(RankingController rankingController) {
		AbstractColorTransformer<?> colorTransformer = (AbstractColorTransformer<?>) rankingController.getModel()
				.getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_COLOR);
		colorTransformer.setColors(new Color[] { new Color(0xFEF0D9), new Color(0xB30000) });
		return colorTransformer;
	}

	/**
	 * Calculating the total shortest path available using
	 * DijkstraShortestPathAlgorithm. Then updates window.
	 */
	void updateShortestPathAvailable(GraphModel graphModel, AttributeModel attributeModel) {
		if (visualizerControls == null)
			return;

		long pathCounter = 0;
		GraphController gc = Lookup.getDefault().lookup(GraphController.class);
		org.gephi.graph.api.Graph undiGephiGraph = gc.getModel().getGraphVisible();
		for (org.gephi.graph.api.Node sourceNode : undiGephiGraph.getNodes()) {
			DijkstraShortestPathAlgorithm dShPath = new DijkstraShortestPathAlgorithm(undiGephiGraph, sourceNode);
			dShPath.compute();
			for (org.gephi.graph.api.Node destNode : undiGephiGraph.getNodes()) {
				if (sourceNode.getId() == destNode.getId()) {
					continue;
				}
				double distance = dShPath.getDistances().get(destNode);
				if (distance != Double.POSITIVE_INFINITY) {
					pathCounter++;
				}
			}
		}

		visualizerControls.setShortestPathCount(pathCounter);
		double netSize = (double) Network.size();
		connValNum++;
		double connectivityPresent = ((double) pathCounter / (netSize * (netSize - 1))) * 100;
		connectivityAvg += connectivityPresent;
		visualizerControls.setConnectivity(connectivityPresent);
		visualizerControls.setAvgConnectivity(connectivityAvg / connValNum);

		GraphDistance gDistance = new GraphDistance();
		// gDistance.setDirected(false);
		gDistance.setDirected(true);
		gDistance.setNormalized(true);
		gDistance.execute(graphModel, attributeModel);

		visualizerControls.setAvShortestPath(gDistance.getPathLength());

	}

	/**
	 * put Network Connected Links on Gephi map
	 */
	private void putNetworkConnectedLinksOnGephiMap(HashMap<Long, it.uniroma1.dis.wsngroup.gexf4j.core.Node> gephiMap) {

		// a map to keep SSID-GOnode
		HashMap<String, Node> ssidMap = new HashMap<>();

		// get all, SSID-GOnode
		for (int i = 0; i < Network.size(); i++) {
			Node node = Network.get(i);
			nodeP2pInfo nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);
			if (nodeInfo.isGroupOwner()) {
				ssidMap.put(nodeInfo.currentGroup.SSID, node);
			}
		}

		// run over all network nodes
		for (int i = 0; i < Network.size(); i++) {
			Node node = Network.get(i);
			nodeP2pInfo nodeInfo = (nodeP2pInfo) node.getProtocol(p2pInfoPid);
			WifiManager wifiManager = (WifiManager) node.getProtocol(wifimanagerPid);

			// check WFD link
			if (!nodeInfo.isGroupOwner() && nodeInfo.getStatus() == CONNECTED) {
				Node go = nodeInfo.getGroupOwner();
				gephiNodeGroup.get(i).connectTo(gephiMap.get(go.getID())).setEdgeType(EdgeType.DIRECTED)
						.setColor(linkWFDColor);
			}

			// check WF link
			if (wifiManager.getWifiStatus() == CONNECTED) {
				Node go = ssidMap.get(wifiManager.apSSID);
				gephiNodeGroup.get(i).connectTo(gephiMap.get(go.getID())).setEdgeType(EdgeType.DIRECTED)
						.setColor(linkWFColor);
			}

			// Linkable idleLinks = (Linkable) node.getProtocol(linkableId);
			// create the gephi layer by setting the neighbors

			// check every possible link
			// for (int j = 0; j < idleLinks.degree(); j++) {
			//
			// //
			// gephiNodeGroup.get(i).connectTo(gephiMap.get(idlelink.getNeighbor(j).getID())).clearColor().setEdgeType(EdgeType.UNDIRECTED).setColor(unusedLinkColor);
			// nodeP2pInfo neighborInfo = (nodeP2pInfo)
			// idleLinks.getNeighbor(j).getProtocol(p2pInfoPid);
			//
			// // make edge based on the current group
			// // this node is group owner and connected to the second node via
			// // wifi p2p interface
			// /*
			// * if(nodeInfo.getStatus()==CONNECTED && nodeInfo.isGroupOwner()
			// * && nodeInfo.currentGroup.getNodeList().contains(idlelink.
			// * getNeighbor(j)) && neighborInfo.getGroupOwner()==node){
			// * gephiNodeGroup.get(i).connectTo(gephiMap.get(idlelink.
			// * getNeighbor(j).getID())).setEdgeType(EdgeType.DIRECTED).
			// * setLabel("WFD").setColor(blackColor);
			// *
			// * // this node is not group owner and the second node is group
			// * owner for this node }else
			// */if (!nodeInfo.isGroupOwner() && nodeInfo.getStatus() ==
			// CONNECTED
			// && nodeInfo.getGroupOwner() == idleLinks.getNeighbor(j)
			// && neighborInfo.currentGroup.getNodeList().contains(node) &&
			// neighborInfo.isGroupOwner()) {
			// gephiNodeGroup.get(i).connectTo(gephiMap.get(idleLinks.getNeighbor(j).getID())).clearColor()
			// .setEdgeType(EdgeType.DIRECTED).setColor(linkWFDColor);
			// //
			// gephiNodeGroup.get(i).connectTo(gephiNodeGroup.get(i).getLabel(),
			// // "qwer", EdgeType.DIRECTED,
			// // gephiMap.get(idlelink.getNeighbor(j).getID()));
			// }
			// }

			// check again for wifi interface as well
			// for (int k = 0; k < idleLinks.degree(); k++) {
			//
			// nodeP2pInfo neighborInfo = (nodeP2pInfo)
			// idleLinks.getNeighbor(k).getProtocol(p2pInfoPid);
			// WifiManager neighborWifiManager = (WifiManager)
			// idleLinks.getNeighbor(k).getProtocol(wifimanagerPid);
			//
			// // this Node is group owner and the second node is connected to
			// // this node via WiFi Interface
			// if (nodeInfo.isGroupOwner() &&
			// nodeInfo.currentGroup.getNodeList().contains(idleLinks.getNeighbor(k))
			// && neighborWifiManager.getWifiStatus() == CONNECTED
			// &&
			// neighborWifiManager.apSSID.equals(nodeInfo.currentGroup.getSSID()))
			// {
			//
			// if
			// (!gephiNodeGroup.get(i).hasEdgeTo(gephiMap.get(idleLinks.getNeighbor(k).getID()).getId()))
			// {
			// gephiNodeGroup.get(i).connectTo(gephiMap.get(idleLinks.getNeighbor(k).getID())).setLabel("WiFi")
			// .setColor(linkWFColor);
			// }
			//
			// // this node (client or group owner) is connected via WiFi
			// // Interface to a Group Owner
			// } else if (wifiManager.getWifiStatus() == CONNECTED
			// && wifiManager.apSSID.equals(neighborInfo.currentGroup.getSSID())
			// && neighborInfo.isGroupOwner()
			// && neighborInfo.currentGroup.getNodeList().contains(node)) {
			//
			// if
			// (!gephiNodeGroup.get(i).hasEdgeTo(gephiMap.get(idleLinks.getNeighbor(k).getID()).getId()))
			// {
			// gephiNodeGroup.get(i).connectTo(gephiMap.get(idleLinks.getNeighbor(k).getID())).clearColor()
			// .setEdgeType(EdgeType.DIRECTED).setColor(linkWFColor);
			// }
			// }
			// }
		}

	}

	/**
	 * 
	 */
	public void printDebug(Object obj) {
		print(obj, Color.black);
	}

	/**
	 * Print object
	 */
	public static void print(Object ob, Color color) {
		if (visualizer != null)
			visualizer.printDebug(ob, color);
	}

	/**
	 * 
	 */
	public static void print(Object obj) {
		if (visualizer != null)
			visualizer.printDebug(obj, Color.black);
	}

	/**
	 * Print object
	 */
	public void printDebug(Object ob, Color color) {
		if (output != null && enableDebugDataToConsole) {
			output.setCaretColor(color);

			output.append("\n" + ob);
			output.setForeground(color);

			if (scrollOutput) {
				output.setCaretPosition(output.getDocument().getLength());
			}
		}
	}

	/**
	 * 
	 */
	public void setShowLinkableConnections(boolean selected) {
		showLinkableConnections = selected;
	}

	/**
	 * 
	 */
	public static Visualizer getVisualizer() {
		return visualizer;
	}

	/**
	 * 
	 */
	public static void incTimeOutInfo() {
		if (visualizer != null)
			visualizer.getControlPane().incTimeOutInfo();
	}

	/**
	 * 
	 */
	private VisualizerControls getControlPane() {
		return visualizerControls;
	}

	/**
	 * 
	 */
	public void updateRoutingStats(AodvStats currentStats) {
		visualizerControls.updateStats(currentStats);

	}

	/**
	 * 
	 */
	public static void incWFDRuleCounter(int ruleNumber) {
		if (visualizer != null)
			visualizer.getControlPane().incWFDRuleCounter(ruleNumber);
	}

	/**
	 * 
	 */
	public static ImageIcon getImageIcon(String filePath) {
		filePath = "icons/" + filePath;
		java.net.URL imgURL = Visualizer.class.getResource(filePath);
		if (imgURL == null) {
			System.err.println("Can't find file: " + filePath);
			return null;
		}
		ImageIcon ic = new ImageIcon(imgURL);
		if (ic.getImageLoadStatus() == java.awt.MediaTracker.COMPLETE) {
			System.out.println("Image: " + filePath + " loaded");
			return ic;
		} else {
			System.err.println("Can't load file: " + filePath);
			return null;
		}
	}

	/**
	 * 
	 */
	public void stepSimulator(boolean b) {
		semPauseSimulator.release();
		try {
			semPauseSimulator.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void setStepUntilSimulator(long cycleNumber) {
		isStepUntilActive = true;
		stepRunUntilCycle = cycleNumber;
		activateStepUntilSimulator();
	}

	/**
	 * 
	 */
	public void setRunUntilSimulator(long cycleNumber) {
		isRunUntilActive = true;
		stepRunUntilCycle = cycleNumber;
		activateStepUntilSimulator();

	}

	/**
	 * 
	 */
	public long getCurrentCycle() {
		return cycle;
	}
}
